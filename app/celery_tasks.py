from . import celery, DOTENV_PATH
from .utils.parsing import markdown_to_dict
from .utils.tree import generate_tree
from flask import current_app
import requests
import dotenv
import os
import json

dotenv.load_dotenv(DOTENV_PATH)


@celery.task
def get_vocab_tree(issues_ep, files_ep, project_id, project_name):
    with current_app.app_context():
        os.environ['REPO_FILES_EP'] = files_ep
        params = {'private_token': os.environ.get('VOCPOPULI_TOKEN'),
                  'scope': 'all',
                  'order_by': 'created_at',
                  'sort': 'desc',
                  'per_page': 100}
        # get all repository issues
        issues_response = requests.get(f"https://gitlab.com/api/v4/{issues_ep}",
                                       params=params)
        issues_dict_list = issues_response.json()
        
        # handle pagination
        for page in range(2, 1 + int(issues_response.headers.get('X-Total-Pages',0))):
            params['page'] = page
            next_page = requests.get(f"https://gitlab.com/api/v4/{issues_ep}",
                                     params=params)
            issues_dict_list += next_page.json()

        issues = issues_dict_list

        all_jsons = []

        # get the list of dicts of the approved terms from the respective issues
        # each dict containts the term's key-value pairs, its issue iid, and
        # its approval status
        approved_terms_dicts = [{**markdown_to_dict(x['description']),
                                 **{'issue_iid': x['iid']},
                                 **{'approved': True}} for x in issues
                                if x['state'] == 'closed'
                                and 'approved' in x['labels']]
        all_jsons += approved_terms_dicts

        # repeat the above step for all candidate terms
        candidate_terms_dicts = [{**markdown_to_dict(x['description']),
                                  **{'issue_iid': x['iid']},
                                  **{'approved': False}} for x in issues
                                 if x['state'] == 'opened'
                                 and 'approved' not in x['labels']]

        # only take the most recent edit into account
        for id_t_global in set([x['id_t_global'] for x in candidate_terms_dicts]):
            candidate_dicts = [x for x in candidate_terms_dicts
                               if x['id_t_global'] == id_t_global]
            all_jsons.append(candidate_dicts[0])
        top_level_terms = [term_dict
                           for term_dict in all_jsons
                           if not (term_dict['broader'])]

        tree_json = generate_tree(vocabulary_terms=all_jsons,
                                  children_list=[[x['id_t_global']
                                                  for x in top_level_terms]],
                                  parents_list=[project_name],
                                  project_id=project_id)

        tree_dict = json.loads(tree_json)
        return tree_dict
