"""
View function wrappers used throughout VocPopuli
"""

from flask_dance.contrib.gitlab import gitlab
from flask import redirect, url_for, session
from functools import wraps
from datetime import datetime, timedelta
import time


def login_required(f):
    """
    Checks whether the current user is logged in GitLab
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not gitlab.authorized:
            return redirect(url_for('gitlab.login_prompt'))
        return f(*args, **kwargs)
    return decorated_function


def update_info(f):
    """
    Updates the cookie which shows the time of the last repository activity
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('PROJECT_SET', False):
            project_info = gitlab.get(f"projects/{session['PROJECT_ID']}")
            utc_time = project_info.json().get('last_activity_at')
            if utc_time != None:
                time_diff = timedelta(hours=int(str(datetime.astimezone(datetime.now()))[-6:-3]))
                t = time.strptime(utc_time.split('.')[0], '%Y-%m-%dT%H:%M:%S')
                t = datetime(t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec) 
                t = t + time_diff
                session['LAST_ACTIVITY'] = t.isoformat() + '.' + utc_time.split('.')[1]

        return f(*args, **kwargs)
    return decorated_function
