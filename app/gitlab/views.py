"""
Views used in the gitlab blueprint
"""

from . import gitlab_blueprint
from flask import render_template


@gitlab_blueprint.route('/login_prompt')
def login_prompt():
    return render_template('gitlab/login.html')
