"""
A module containing wrapper functions for the GitLab API.

Where needed, different methods for taking care of paginated
results are implemented. For more information, visit
https://docs.gitlab.com/ee/api/#pagination
"""

from typing import Dict, List
from .repository_endpoints import Endpoints
from ..utils.license import CC_BY_40
from .. import DOTENV_PATH
from flask_dance.contrib.gitlab import gitlab
import re
import dotenv
import os
import json

dotenv.load_dotenv(DOTENV_PATH)


def get_branches() -> List[Dict]:
    """
    Get a list of all of the branches' dictionaries in a given repository
    """
    ep = Endpoints()
    branches_response = gitlab.get(ep.REPO_BRANCHES_EP,
                                   params={'per_page': 100})
    branches_dict_list = branches_response.json()

    # handle pagination

    for page in range(2, 1 + int(branches_response.headers['X-Total-Pages'])):
        next_page = gitlab.get(ep.REPO_BRANCHES_EP, params={'per_page': 100,
                                                            'page': page})
        branches_dict_list += next_page.json()

    return branches_dict_list


def get_branch_names(drop_merged=True) -> List[str]:
    """
    Get the names of all branches in a given repository
    """
    branches_dict_list = get_branches()
    if drop_merged:  # disregard already merged branches
        names = [branch_dict['name']
                 for branch_dict in branches_dict_list
                 if not branch_dict['merged']]
    else:
        names = [branch_dict['name']
                 for branch_dict in branches_dict_list]

    return names


def post_new_term_issue(title: str,
                        description: str,
                        labels: str = None,
                        subscribe: bool = True) -> bool:
    """
    Post a new issue related to a given term.
    """
    ep = Endpoints()
    # the data passed to the POST request
    request_data = {
        "title": title,  # issue title
        "description": description  # issue description
        }

    # add any issue labels, if applicable
    if labels:
        request_data["labels"] = labels

    # POST the new issue
    response = gitlab.post(f"{ep.ISSUES_EP}",
                           data=request_data)
    # if response.ok and subscribe:  # subscribe to the issue
    #     new_issue_iid = response.json()['iid']
    #     gitlab.post(f"{ep.ISSUES_EP}/{new_issue_iid}/subscribe")

    return response


def create_new_vocab_repository(repository_name: str,
                                repository_description: str,
                                remove_branch_after_merge: bool = False,
                                visibility: str = "private") -> Dict:
    """
    Attempts to create a new vocabulary repository.
    """
    response = gitlab.post(
        'projects',
        data={
            "name": repository_name,
            "description": repository_description,
            "initialize_with_readme": False,
            "default_branch": "main",
            "remove_source_branch_after_merge": remove_branch_after_merge,
            "visibility": visibility
            }
        )

    if response.json().get('id', False):
        # Try adding the VocPopuli admin to the repository's members
        if os.getenv('VOCPOPULI_ADMIN_USER_ID'):
            admin_id = int(os.getenv('VOCPOPULI_ADMIN_USER_ID'))
            gitlab.post(f"projects/{response.json()['id']}/members",
                        data={"user_id": admin_id,
                              "access_level": 50})  # owner

        # Create an approved_terms, candidate_terms, and an images
        # directory in the new repository. Additionally, add
        # a vocabulary license, and metadata file.
        approved_terms_ep = (f"projects/{response.json()['id']}/"
                             f"repository/files/approved_terms%2F%2Egitkeep")
        candidate_terms_ep = (f"projects/{response.json()['id']}/"
                              f"repository/files/candidate_terms%2F%2Egitkeep")
        images_ep = (f"projects/{response.json()['id']}/"
                     f"repository/files/images%2F%2Egitkeep")
        license_ep = (f"projects/{response.json()['id']}/"
                      f"repository/files/LICENSE")
        metadata_ep = (f"projects/{response.json()['id']}/"
                       f"repository/files/metadata%2Ejson")
        readme_ep = (f"projects/{response.json()['id']}/"
                     f"repository/files/README%2Emd")

        # TODO: add IRI's
        metadata_str = json.dumps({"VOCABULARY_NAME": repository_name,
                                   "VOCABULARY_PROV_PURL-IRI": "",
                                   "VOCABULARY_SKOS_PURL-IRI": ""},
                                  indent=4)
        gitlab.post(
            license_ep,
            data={"branch": "main",
                  "content": CC_BY_40,
                  "commit_message": "Add LICENSE"}
        )
        gitlab.post(
            readme_ep,
            data={"branch": "main",
                  "content": repository_description,
                  "commit_message": "Add README.md"}
        )
        gitlab.post(
            metadata_ep,
            data={"branch": "main",
                  "content": metadata_str,
                  "commit_message": "Add metadata.json"}
        )
        gitlab.post(
            approved_terms_ep,
            data={"branch": "main",
                  "content": "",
                  "commit_message": "Create approved_terms"}
        )

        gitlab.post(
            candidate_terms_ep,
            data={"branch": "main",
                  "content": "",
                  "commit_message": "Create candidate_terms"}
        )

        gitlab.post(
            images_ep,
            data={"branch": "main",
                  "content": "",
                  "commit_message": "Create images"}
        )
    return response.json()


def add_new_file(file: str,
                 file_path: str,
                 branch: str = 'main',
                 start_branch: str = 'main',
                 commit_message='New term definition') -> bool:
    """
    Add a new file to the repository.
    """
    ep = Endpoints()
    response = gitlab.post(f"{ep.REPO_FILES_EP}/{file_path}",
                           data={"branch": branch,
                                 "start_branch": start_branch,
                                 "content": file,
                                 "commit_message": commit_message})
    return response.ok


def add_new_branch(branch: str, ref: str) -> bool:
    """
    Create a new branch. Optionally, from an existing one.

    branch --- Name of the new branch.

    ref --- Branch name or commit SHA to create the new branch from.
    """
    ep = Endpoints()
    response = gitlab.post(f"{ep.REPO_BRANCHES_EP}",
                           data={"branch": branch,
                                 "ref": ref})
    return response.ok


def get_issues_list(params: Dict) -> List[Dict]:
    """
    Get a list of the dictionaries of each all project issues
    """
    ep = Endpoints()
    issues_response = gitlab.get(f"{ep.ISSUES_EP}", params=params)
    issues_dict_list = issues_response.json()

    # handle pagination
    for page in range(2, 1 + int(issues_response.headers['X-Total-Pages'])):
        params['page'] = page
        next_page = gitlab.get(f"{ep.ISSUES_EP}", params=params)
        issues_dict_list += next_page.json()
    return issues_dict_list


def get_branch_tree(branch: str = 'main') -> List[Dict]:
    """
    Get the tree of a given repository branch.
    The tree contains, among others, the paths of all files
    in the branch.
    """
    ep = Endpoints()
    branch_tree_dicts = []
    branch_files_response = gitlab.get(f'{ep.REPO_TREE_EP}',
                                       params={'recursive': 'true',
                                               'ref': branch,
                                               'per_page': 100,
                                               'pagination': 'keyset'})

    branch_tree_dicts += branch_files_response.json()

    # handle pagination
    if 'rel="next"' in branch_files_response.headers['Link']:
        more_pages_left = True
    else:
        more_pages_left = False

    while more_pages_left:
        # get all pagination links
        pagination_links = (branch_files_response.headers['Link']
                            .split(', '))
        # get the link to the next page
        next_page_link = [x
                          for x in pagination_links
                          if 'rel="next"' in x][0]
        # strip 'rel="next"'
        next_page_link = next_page_link.split(';')[0]
        # remove leading < and trailing >
        next_page_link = next_page_link[1:-1]
        # strip the API URL
        next_page_link = re.sub('https://gitlab.com/api/v4/',
                                '',
                                next_page_link)
        branch_files_response = gitlab.get(next_page_link)
        branch_tree_dicts += branch_files_response.json()

        # breaking condition
        if 'rel="next"' in branch_files_response.headers['Link']:
            more_pages_left = True
        else:
            more_pages_left = False

    return branch_tree_dicts
