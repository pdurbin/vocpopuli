from typing import List
import os
try:
    from collections.abc import Iterable
except ImportError:
    from collections import Iterable
from flask_wtf import FlaskForm
from wtforms import (StringField, SubmitField, SelectMultipleField,
                     SelectField, TextAreaField, MultipleFileField,
                     BooleanField, IntegerField)
from wtforms.validators import (DataRequired, NoneOf,
                                ValidationError, Optional)
from werkzeug.datastructures import FileStorage

MAX_IMG_FILE_SIZE_MB = (os.environ.get('MAX_IMG_FILE_SIZE_MB') or 2)


def file_size(size_limit_mb=2):

    def _file_size(form, field):
        if not (all(isinstance(item, FileStorage) for item in field.data)
                and field.data):
            return
        if len(field.data) == 1 and field.data[0].filename == '':
            return
        for x in field.data:
            if len(x.read()) > size_limit_mb*(1024**2):  # convert MB to byte
                raise ValidationError(f"File should be smaller than {size_limit_mb} MB")
            x.seek(0)
    return _file_size


def is_http_uri():

    def _is_http_uri(form, field):
        if field.data is not None:
            values = field.data.split(',')
            values = [x.lstrip() for x in values]
            all_start_with_http = all([x.startswith('http') for x in values])
            if not all_start_with_http:
                raise ValidationError("All URIs should start with http")
            else:
                return
        else:
            return

    return _is_http_uri


class TermsJSONsUploadForm(FlaskForm):
    vocab_name = StringField("New vocabulary name (optional):")

    files = MultipleFileField("Select the JSON files"
                              " of the new vocabulary's terms."
                              " If left empty, an empty"
                              " vocabulary repository will"
                              " be created")

    submit = SubmitField('Submit')


class AddNewCommentForm(FlaskForm):
    comment = TextAreaField('Add a new comment:',
                            validators=[DataRequired()],
                            render_kw={'style': "height:150px;",
                                       'class': 'comment-area'})
    submit = SubmitField('Submit comment',
                         render_kw={'class': 'btn btn-colour comment-btn'})


class NewTermForm(FlaskForm):
    none_of_msg = 'The term label is already present in the repository'

    label = StringField('Term:',
                        validators=[
                                    DataRequired(),
                                    NoneOf(values=[], message=none_of_msg)
                                    ],
                        render_kw={'placeholder': 'Unique name'})

    definition = TextAreaField('Definition:',
                               validators=[DataRequired()],
                               render_kw={'placeholder': 'Term definition',
                                          'rows': 3})

    s_descr = "Comma-separated values with optional language tags (@de, @fr)."
    synonym = StringField('Synonyms & translations:',
                          description=s_descr,
                          render_kw={'placeholder': 'E.g.: cup, Tasse@de'})

    datatype = SelectField(label='Data type:',
                           description='If applicable',
                           choices=[(' ', 'none'),
                                    ('record', 'record'),
                                    ('boolean', 'boolean'),
                                    ('integer', 'integer'),
                                    ('float', 'float'), ('string', 'string'),
                                    ('date', 'date'), ('list', 'list'),
                                    ('dictionary', 'dictionary'),
                                    ('option', 'option')])
    contextual_type = SelectField(label='Contextual type:',
                                  choices=[('process', 'process'),
                                           ('object', 'object'),
                                           ('data', 'data')],
                                  validators=[Optional()])
    broader = SelectMultipleField('Relatively broader terms:')
    related = SelectMultipleField('Related terms (non-hierarchical):')
    r_e_descr = "Comma-separated URIs beginning with 'http'"
    related_external = StringField('Related external terms:',
                                   description=r_e_descr)
    images = MultipleFileField("Image upload (optional)",
                               validators=[file_size(MAX_IMG_FILE_SIZE_MB)])
    keep_prev_imgs = BooleanField("Keep the previous version's images")

    submit = SubmitField('Submit term')

    def __init__(self, all_terms: List[str] = None,
                 broader_choices: List[str] = None,
                 related_choices: List[str] = None,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label.validators[1].values = all_terms
        self.broader.choices = broader_choices
        self.related.choices = related_choices


class KadiTemplateExport(FlaskForm):
    template = SelectField('Choose Template:')
    title_template = StringField('Title:', validators=[DataRequired()])
    tags = StringField("Tags:", render_kw={'placeholder': 'comma separated tags, e.g.,  one, two'})
    description = TextAreaField("Description:", render_kw={'rows': 3})

    host = StringField("Kadi4Mat Host:", default='https://kadi4mat.iam-cms.kit.edu')
    pat = StringField("Kadi4Mat Personal Access Token:")

    submit = SubmitField('Export Template to Kadi4Mat')

    def __init__(self, possible_templates: List[str] = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.template.choices = possible_templates


class PublishVocabularyForm(FlaskForm):
    namespace = SelectField('GitLab™ group/namespace:',
                            validators=[DataRequired()])
    vocabulary_tag = StringField("Vocabulary tag:",
                                 render_kw={'placeholder': 'A tag of max. 7 characters. E.g., triboex'},
                                 validators=[DataRequired()])

    submit = SubmitField('Publish to GitLab™')

    def __init__(self, namespaces, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.namespace.choices = namespaces
