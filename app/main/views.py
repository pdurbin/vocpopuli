"""
Views used in the main blueprint
"""

from . import main
from .. import celery
from .forms import (NewTermForm, AddNewCommentForm,
                    TermsJSONsUploadForm, KadiTemplateExport,
                    PublishVocabularyForm)
from .. import DOTENV_PATH
from ..gitlab_api.repository_endpoints import Endpoints
from ..gitlab_api import api
from ..decorators import login_required, update_info
from ..celery_tasks import get_vocab_tree
from ..utils.terms import get_term_jsons, generate_uid, term_tag
from ..utils.tree import generate_tree
from ..utils.parsing import (markdown_to_dict, new_term_to_markdown,
                             pre_jsonify_form)
from ..utils.repo import load_repositories
from ..utils.skos import SKOSSerializer
from ..utils.license import CC_BY_40
from flask import (redirect, render_template, url_for,
                   request, session, Response, current_app)
from flask_dance.contrib.gitlab import gitlab
from werkzeug.datastructures import FileStorage

from git import Repo

from kadi_apy import KadiManager
from datetime import datetime, timedelta
from shutil import copytree, move
import dotenv

import hashlib
import re
import os
import json
import time
import uuid
import requests

dotenv.load_dotenv(DOTENV_PATH)
# Allowed image file extensions for image upload
img_filetypes = ['.png', '.jpg', '.jpeg ']

# Dictionaries used in defining the colors of
# different datatype tags and icons in the list_terms view
dtype_icon_map = {'boolean': 'bi-plus-slash-minus',
                  'integer': 'bi-123',
                  'float': 'bi-dot',
                  'string': 'bi-file-text',
                  'date': 'bi-calendar-plus',
                  'list': 'bi-list-ul',
                  'dictionary': 'bi-book',
                  'individual': 'bi-person',
                  'option': 'bi-circle-fill',
                  'record': 'bi-journals',
                  'none': 'bi-dash-circle-dotted'}
dtype_color_map = {'boolean': '#44bcd8',
                   'integer': '#e28743',
                   'float': '#eab676',
                   'string': '#1e81b0',
                   'date': '#358166',
                   'list': '#154c79',
                   'dictionary': '#365985',
                   'individual': '#76b5c5',
                   'option': '#76b5c5',
                   'record': '#707070',
                   'none': '#808080'}


@update_info
def _submit_new_term(form):
    """
    A wrapper function which takes the submitted form
    containing a new term's info, and creates a term from it.
    """
    # get logged-in user's info
    user = gitlab.get('user').json()

    # preprocess the term form data:
    pre_json_data = pre_jsonify_form(form.data)
    # generate term metadata
    term_metadata = {}
    term_metadata['created_by_user_id'] = user['id']
    term_metadata['created_on'] = (datetime
                                   .utcnow()
                                   .isoformat())
    term_metadata['edited_by_user_id'] = term_metadata['created_by_user_id']
    term_metadata['edited_on'] = term_metadata['created_on']
    term_metadata['id_t_global'] = (term_tag(pre_json_data.get('label')) +
                                    '-' + generate_uid() +
                                    '-vp')
    term_metadata['id_t_local'] = ""
    term_metadata['id_t_version'] = generate_uid() + '-vp'

    # generate the term's dictionary
    term_dict = {}
    term_dict['metadata'] = term_metadata
    term_dict['term'] = pre_json_data

    # create a new repository branch and commit the new term definition as
    # a json file
    id_t_global = term_dict['metadata']['id_t_global']
    branch_name = f"new_term_{id_t_global}"
    term_label = term_dict['term']['label']

    commit_message = (f"New term definition: {term_label}. "
                      f"Global Term ID: {id_t_global}")

    # commit the file in a new branch
    vocabulary_name = session.get('PROJECT_NAME', '').replace(' ', '-')
    main_branch_dir = os.path.join(current_app.config['VOCABULARIES_DIR'],
                                   vocabulary_name,
                                   f"{vocabulary_name}-main")

    new_branch_dir = os.path.join(current_app.config['VOCABULARIES_DIR'],
                                  vocabulary_name,
                                  f"{vocabulary_name}-{branch_name}")
    main_branch_repo = Repo(main_branch_dir)
    main_branch_repo.git.pull()
    copytree(main_branch_dir, new_branch_dir)

    # Initialize the new branch's repository
    new_branch_repo = Repo(new_branch_dir)
    # Create and checkout the new branch
    new_branch_repo.git.checkout('-b', branch_name)
    # Set the creator's credentials
    (new_branch_repo.config_writer()
     .set_value("user", "name", user['name']).release())
    (new_branch_repo.config_writer()
     .set_value("user", "email", user['email']).release())

    # Upload images (if such exist), and update JSON
    img_files = [x for x in form.images.data if isinstance(x, FileStorage)]
    valid_img_upload_urls = []
    base_url = session.get('BASE_URL')

    for i, img in enumerate(img_files):
        if any(img.filename.lower().endswith(x) for x in img_filetypes):

            img_file_name = f"{id_t_global}_{i}"
            _, ext = os.path.splitext(img.filename.lower())
            img_path = f"images/{img_file_name}{ext}"

            img.save(os.path.join(new_branch_repo.working_tree_dir,
                                  img_path))

            img_url = f"{base_url}/-/raw/{branch_name}/images/{img_file_name}{ext}"
            valid_img_upload_urls.append(img_url)

    # update the term JSON with the image file names
    term_dict['image_files'] = [x for x in valid_img_upload_urls]

    # Create the issue
    term_markdown = new_term_to_markdown(term_dict)
    issue_title = f'[New Term] {term_label} | {id_t_global}'
    issue_resp = api.post_new_term_issue(title=issue_title,
                                         description=term_markdown,
                                         labels="new_term")

    term_dict['metadata']['issue_iid'] = issue_resp.json()['iid']

    # upload the term's JSON
    json_str = json.dumps(term_dict, indent=4)
    json_str += ' \n'
    json_file = json.loads(json_str)

    # the term's file path
    file_path = f"candidate_terms/{id_t_global}.json"
    with open(os.path.join(new_branch_repo.working_tree_dir, file_path), 'w+') as f:
        json.dump(json_file, f, indent=4)

    # Stage the new changes, and commit
    new_branch_repo.git.add('.')
    new_branch_repo.git.commit('-m', commit_message,
                               author=f"{user['name']} <{user['email']}>")
    # TODO: optional push
    new_branch_repo.git.push('-u', 'origin', branch_name)

    return redirect(url_for('.list_terms'))


@main.route('/')
def root():
    if not gitlab.authorized:
        return redirect(url_for('gitlab.login_prompt'))
    else:
        return redirect(url_for('main.index'))


@main.route('/choose_project', methods=['POST'])
@login_required
def choose_project():
    # load the repositories which the user can choose from
    user_projects = gitlab.get('projects',
                               params={'membership': True})
    user_projects_names = [x['name'] for x in user_projects.json()]
    # check whether the current set of repositories,
    # which the user has access to
    # is the same as the one stored in the session cookie
    # if not -> update the session cookie
    if set(user_projects_names) != set(session.get('USER_PROJECTS', [-1])):
        vocab_projects = {}
        for repo in user_projects.json():
            repo_id = repo['id']
            repo_tree = gitlab.get(f'projects/{repo_id}/repository/tree')
            top_level_paths = [x['path']
                               for x in repo_tree.json()]
            if 'approved_terms' in top_level_paths and 'candidate_terms' in top_level_paths:
                vocab_projects[repo['name']] = repo_id

        session['USER_PROJECTS'] = [x['name'] for x in user_projects.json()]
        session['PROJECTS_DICT'] = vocab_projects
        if (request.form.get('vocab_select', "Choose a vocabulary project:")
                != "Choose a vocabulary project:"):
            # store the repository information in the session cookie
            session['PROJECT_ID'] = vocab_projects[request.form.get('vocab_select')]
            session['PROJECT_NAME'] = request.form.get('vocab_select')
            session['PROJECT_SET'] = True
            session['tree_json'] = None
    else:
        session['USER_PROJECTS'] = user_projects_names

        if (request.form.get('vocab_select', "Choose a vocabulary project:")
                != "Choose a vocabulary project:"):
            # store the repository information in the session cookie
            session['PROJECT_ID'] = session['PROJECTS_DICT'][request.form.get('vocab_select')]
            session['PROJECT_NAME'] = request.form.get('vocab_select')
            session['PROJECT_SET'] = True
            session['tree_json'] = None
    # TODO: pull repo if not already there
    session['BASE_URL'] = gitlab.get(f"projects/{session['PROJECT_ID'] }").json()['web_url']
    return redirect(request.referrer)


@main.route('/index')
@login_required
@update_info
def index():
    # load the repositories which the user can choose from
    user_projects = gitlab.get('projects',
                               params={'membership': True})

    user_projects_names = [x['name'] for x in user_projects.json()]

    # check whether the current set of repositories,
    # which the user has access to
    # is the same as the one stored in the session cookie
    # if not -> update the session cookie
    if set(user_projects_names) != set(session.get('USER_PROJECTS', [-1])):
        vocab_projects = {}
        for repo in user_projects.json():
            repo_id = repo['id']
            repo_tree = gitlab.get(f'projects/{repo_id}/repository/tree')
            if repo_tree.ok:
                top_level_paths = [x['path']
                                for x in repo_tree.json()]
                if 'approved_terms' in top_level_paths and 'candidate_terms' in top_level_paths:
                    vocab_projects[repo['name']] = repo_id

        session['USER_PROJECTS'] = [x['name'] for x in user_projects.json()]
        session['PROJECTS_DICT'] = vocab_projects
    else:
        session['USER_PROJECTS'] = [x['name'] for x in user_projects.json()]
        session['PROJECTS_DICT'] = session['PROJECTS_DICT']
    return render_template('index.html')


@main.route('/child_term/<int:issue_iid>/<path>', methods=['GET', 'POST'])
@login_required
def child_term(issue_iid, path):
    ep = Endpoints()

    # get the parent term's issue and extract the relevant data
    issue = gitlab.get(f"{ep.ISSUES_EP}/{issue_iid}")
    if not issue.ok:
        return redirect(url_for('.view_term', issue_iid=issue_iid, path=path))
    parent_label = issue.json()['title'].split('] ')[1]
    parent_label = parent_label.split(' | ')[0]
    parent_id_t_global = issue.json()['title'].split(' | ')[1]

    tree_dict = session.get(f"tree_dict_{session.get('PROJECT_NAME', 'nan')}")
    if not session.get(f"all_terms_{session.get('PROJECT_NAME', 'nan')}"):
        all_jsons = get_term_jsons()
        all_terms = [(f"{x['id_t_global']} | {x['label']}", x['label'])
                     for x in all_jsons]
        session[f"all_terms_{session.get('PROJECT_NAME', 'nan')}"] = all_terms
    else:
        all_terms = session[f"all_terms_{session.get('PROJECT_NAME', 'nan')}"]

    ontology_terms = []  # TODO: get from ontology
    term_choices = (ontology_terms
                    + all_terms)
    parent_dict = markdown_to_dict(issue.json()['description'])
    parent_datatype = parent_dict['datatype']
    if parent_datatype in ['string', 'boolean', 'integer', 'float', 'date']:
        form = NewTermForm(datatype='option',
                           all_terms=[x[1] for x in all_terms],  # just labels
                           broader_choices=term_choices,
                           related_choices=term_choices,
                           broader=[(f"{parent_id_t_global} | {parent_label}")])
        # form.datatype.render_kw = {'disabled': 'true'}
    else:
        form = NewTermForm(all_terms=[x[1] for x in all_terms],  # just labels,
                           broader_choices=term_choices,
                           related_choices=term_choices,
                           broader=[(f"{parent_id_t_global} | {parent_label}")])

    # check if submission is valid and submit new term
    if form.validate_on_submit():
        session.pop(f"all_terms_{session.get('PROJECT_NAME', 'nan')}")
        return _submit_new_term(form)

    return render_template('child_term.html',
                           form=form,
                           issue_iid=int(issue_iid),
                           tree_dict=tree_dict)


@main.route('/new_term', methods=['GET', 'POST'])
@login_required
def new_term():
    """
    The view function used in the creation of a new term.
    It renders an instance of NewTermForm, submits its data to
    the respective vocabulary's repository, and creates a new issue
    which reflects the creation of a new term.
    """

    tree_dict = session.get(f"tree_dict_{session.get('PROJECT_NAME', 'nan')}")
    if not session.get(f"all_terms_{session.get('PROJECT_NAME', 'nan')}"):
        all_jsons = get_term_jsons()
        all_terms = [(f"{x['id_t_global']} | {x['label']}", x['label'])
                     for x in all_jsons]
        session[f"all_terms_{session.get('PROJECT_NAME', 'nan')}"] = all_terms
    else:
        all_terms = session[f"all_terms_{session.get('PROJECT_NAME', 'nan')}"]

    ontology_terms = []  # TODO: get from ontology
    term_choices = (ontology_terms
                    + all_terms)

    form = NewTermForm(all_terms=[x[1] for x in all_terms],  # just labels,
                       broader_choices=term_choices,
                       related_choices=term_choices)

    # check if submission is valid and submit new term
    if form.validate_on_submit():
        session.pop(f"all_terms_{session.get('PROJECT_NAME', 'nan')}")
        return _submit_new_term(form)

    return render_template('new_term.html',
                           form=form,
                           tree_dict=tree_dict)


@main.route('/edit_term/<int:issue_iid>/<path>', methods=['GET', 'POST'])
@login_required
@update_info
def edit_term(issue_iid, path):
    """
    The view function used to edit a given term.

    issue_iid --- The IID of the issue linked to the version of the
    term which the user wants to edit.
    """
    ep = Endpoints()
    # get the edited term's issue and extract the relevant data
    issue = gitlab.get(f"{ep.ISSUES_EP}/{issue_iid}")

    # basic error handling
    if not issue.ok:
        return redirect(url_for('.view_term', issue_iid=issue_iid, path=path))

    # check if the term has been approved
    is_term_approved = (issue.json()['state'] == 'closed')

    # get data related to the term's definition
    term_definition = issue.json()['description']
    issue_title = issue.json()['title']

    # get all term labels in the entire repository
    if not session.get(f"all_terms_{session.get('PROJECT_NAME', 'nan')}"):
        all_jsons = get_term_jsons()
        all_terms = [(f"{x['id_t_global']} | {x['label']}", x['label'])
                     for x in all_jsons]
        session[f"all_terms_{session.get('PROJECT_NAME', 'nan')}"] = all_terms
    else:
        all_terms = session[f"all_terms_{session.get('PROJECT_NAME', 'nan')}"]
    ontology_terms = []  # TODO: get from ontology
    term_choices = (ontology_terms
                    + all_terms)

    # convert the issue's markdown to a dict
    form_data_dict = markdown_to_dict(term_definition=term_definition)
    # get all prior labels of the edited term so that it cannot
    # add itself as a broader term
    all_term_issues = api.get_issues_list(
        params={'search': form_data_dict['id_t_global'],  # only issues containing id_t_global
                'order_by': 'created_at',
                'sort': 'desc',
                'per_page': 100
                })
    all_term_issues = [x for x in all_term_issues
                       if form_data_dict['id_t_global'] in x['title']]
    all_term_labels = []
    for x in all_term_issues:
        label = (x['title'].split('] ')[1]).split(' | ')[0]
        global_id = x['title'].split(' | ')[1]
        all_term_labels.append((f"{global_id} | {label}", label))
    all_term_labels = list(set(all_term_labels))

    for prev_label in all_term_labels:
        if prev_label in term_choices:
            term_choices.remove(prev_label)
    if (f"{form_data_dict['id_t_global']} | {form_data_dict['label']}", form_data_dict['label']) in term_choices:
        term_choices.remove((f"{form_data_dict['id_t_global']} | {form_data_dict['label']}", form_data_dict['label']))

    related_external_terms = form_data_dict.get('related_external', [])
    related_external_terms = ','.join([x for x in related_external_terms])
    broader = [(f"{i} | {l}") for i, l in zip(form_data_dict['broader'], form_data_dict['broader_labels'])]
    related = [(f"{i} | {l}") for i, l in zip(form_data_dict['related'], form_data_dict['related_labels'])]
    # fill the form for editing the term
    form = NewTermForm(label=form_data_dict['label'],
                       all_terms=[x[1] for x in all_terms],  # just labels,
                       broader_choices=term_choices,
                       related_choices=term_choices,
                       datatype=form_data_dict['datatype'],
                       contextual_type=form_data_dict['contextual_type'],
                       broader=broader,
                       related=related,
                       related_external=related_external_terms)
    # form.label.data = form_data_dict['label']
    form.label.validators[1].values = []

    tree_dict = session.get(f"tree_dict_{session.get('PROJECT_NAME', 'nan')}")

    if request.method == 'GET':
        form.definition.data = form_data_dict['definition']
        form.synonym.data = ', '.join(x for x in form_data_dict['synonyms'])

    # check if submission is valid and submit new term
    if form.validate_on_submit():
        session.pop(f"all_terms_{session.get('PROJECT_NAME', 'nan')}")
        user = gitlab.get('user').json()
        vocabulary_name = session['PROJECT_NAME'].replace(' ', '-')
        vocabulary_branches_path = os.path.join(
                current_app.config['VOCABULARIES_DIR'],
                vocabulary_name
                )
        # get the name of the respective term JSON (if not approved)
        if '[New Term]' in issue_title:
            term_file_name = (form_data_dict['id_t_global'] + '.json')

        elif '[Term Edit #' in issue_title:
            start_idx = issue_title.find('#') + 1
            end_idx = issue_title.find(']')
            edit_n = int(issue_title[start_idx:end_idx])

            term_file_name = (form_data_dict['id_t_global'] +
                              '_edit_' +
                              str(edit_n) +
                              '.json')
        keep_old_images = form.keep_prev_imgs.data
        if is_term_approved:
            # if the term to be edited has already been approved,
            # it needs to be reset to an unapproved version
            # this includes the following steps:

            # 1. create the new .json which reflects the edit
            main_branch_name = "main"
            main_branch_dir = os.path.join(vocabulary_branches_path,
                                           f"{vocabulary_name}-{main_branch_name}")
            approved_term_file_path = os.path.join(main_branch_dir,
                                                   "approved_terms",
                                                   f"{form_data_dict['id_t_global']}.json")

            with open(approved_term_file_path) as f:
                approved_term_file = json.load(f)
            prev_term_metadata = approved_term_file['metadata']

            # preprocess the form data:
            pre_json_data = pre_jsonify_form(form.data)

            # generate term metadata
            term_metadata = {}
            term_metadata['created_by_user_id'] = prev_term_metadata['created_by_user_id']
            term_metadata['created_on'] = prev_term_metadata['created_on']
            term_metadata['edited_by_user_id'] = user['id']
            term_metadata['edited_on'] = (datetime
                                          .utcnow()
                                          .isoformat())
            term_metadata['id_t_global'] = (prev_term_metadata.get('id_t_global',
                                            (term_tag(pre_json_data.get('label')) +
                                             '-' + generate_uid() +
                                             '-vp'))
                                            )

            term_metadata['id_t_local'] = ""
            term_metadata['id_t_version'] = generate_uid() + '-vp'

            term_metadata['generated_from_id_t_version'] = prev_term_metadata.get('id_t_version',
                                                                                  generate_uid() + '-vp')
            term_metadata['generated_from_file_name'] = term_file_name
            # generate the term's dictionary
            term_dict = {}
            term_dict['metadata'] = term_metadata
            term_dict['term'] = pre_json_data
            # if record moved under another term, remove contexual type
            if term_dict['term']['contextual_type'] != '' and len(term_dict['term']['broader']) != 0:
                term_dict['term']['contextual_type'] = ''
            id_t_global = term_dict['metadata']['id_t_global']
            if keep_old_images:
                term_dict['image_files'] = approved_term_file['image_files']
                valid_img_upload_urls = term_dict['image_files']

            # 2. create a new branch reflecting the re-edit
            # and move the previously approved .JSON in the
            # candidate_terms folder of the new branch
            all_branch_names = api.get_branch_names(drop_merged=False)
            if len(all_branch_names) == 0:
                all_branch_names = api.get_branch_names(drop_merged=False)

            relevant_branches = [x for x in all_branch_names
                                 if id_t_global in x]
            n_reopenings = len([x for x in relevant_branches
                                if "term_reopening_" in x])
            n_relevant_branches = len(relevant_branches)
            new_branch_name = f"term_reopening_{n_reopenings+1}_{id_t_global}"

            if n_relevant_branches == 1:
                last_branch_name = relevant_branches[0]
            elif n_relevant_branches == 0:
                last_branch_name = "main"
            else:
                last_branch_name = f"term_reopening_{n_reopenings}_{id_t_global}"

            new_branch_commit_message = f"Reopened {id_t_global} for editing."
            last_branch_dir = os.path.join(vocabulary_branches_path,
                                           f"{vocabulary_name}-{last_branch_name}")
            # the path to the approved JSON in its previous branch
            last_branch_file_path = os.path.join(vocabulary_branches_path,
                                                 f"{vocabulary_name}-{new_branch_name}",  # used after copy
                                                 "approved_terms",
                                                 f"{id_t_global}.json")

            new_branch_dir = os.path.join(vocabulary_branches_path,
                                          f"{vocabulary_name}-{new_branch_name}")
            # the path of the previously approved JSON in the new branch (w/ _edit_n.json)
            new_branch_file_path = os.path.join(vocabulary_branches_path,
                                                f"{vocabulary_name}-{new_branch_name}",
                                                "candidate_terms",
                                                f"{term_file_name}")
            last_branch_repo = Repo(last_branch_dir)
            last_branch_repo.git.checkout(last_branch_name)
            copytree(src=last_branch_dir, dst=new_branch_dir)
            new_branch_repo = Repo(new_branch_dir)
            new_branch_repo.git.checkout('-b', new_branch_name)
            (new_branch_repo.config_writer()
             .set_value("user", "name", user['name']).release())
            (new_branch_repo.config_writer()
             .set_value("user", "email", user['email']).release())

            move(src=last_branch_file_path, dst=new_branch_file_path)
            new_branch_repo.git.add('.')
            new_branch_repo.git.commit('-m', new_branch_commit_message)
            # 3. Create a new .JSON in the new branch which reflects the edit

            # get the consecutive number of the edit
            candidate_files = os.listdir(os.path.join(new_branch_dir,
                                                      'candidate_terms')
                                         )
            candidate_files = [x for x in candidate_files
                               if id_t_global in x]
            candidate_files = [x for x in candidate_files
                               if x.endswith('.json')]
            edit_count = len([x for x in candidate_files
                              if '_edit_' in x])
            edit_count += 1

            term_label = term_dict['term']['label']
            commit_message = (f"Term edit #{edit_count}: {term_label}. "
                              f"Global Term ID: {id_t_global}")

            # Upload images (if such exist), and update JSON
            if not keep_old_images:
                # get the number of previous image files for this term
                branch_img_dir = os.path.join(new_branch_dir, 'images')
                branch_images = os.listdir(branch_img_dir)
                branch_images = [x for x in branch_images
                                 if x != '.gitkeep']
                prev_imgs = [re.split("\_[0-9]+.*", x)[0] for x in branch_images]
                prev_imgs = [x for x in prev_imgs if id_t_global in x]
                n_prev_imgs = len(prev_imgs)

                # Upload images (if such exist), and update JSON
                img_files = [x for x in form.images.data if isinstance(x, FileStorage)]
                valid_img_upload_urls = []
                base_url = session.get('BASE_URL')
                for i, img in enumerate(img_files):
                    if any(img.filename.lower().endswith(x) for x in img_filetypes):
                        img_file_name = f"{id_t_global}_{i+n_prev_imgs}"
                        _, ext = os.path.splitext(img.filename.lower())
                        img_path = f"images/{img_file_name}{ext}"
                        img.save(os.path.join(new_branch_dir,
                                              img_path))
                        img_url = f"{base_url}/-/raw/{new_branch_name}/images/{img_file_name}{ext}"
                        valid_img_upload_urls.append(img_url)

                # update the term JSON with the image file names
                term_dict['image_files'] = [x for x in valid_img_upload_urls]

            # Create the issue
            term_markdown = new_term_to_markdown(term_dict)
            new_issues_response = api.post_new_term_issue(
                title=f"[Term Edit #{edit_count}] {term_label} | {id_t_global}",
                description=term_markdown,
                labels="term_edit, term_reopening")
            new_issue_iid = new_issues_response.json()['iid']
            new_issue_iid = int(new_issue_iid)

            term_dict['metadata']['issue_iid'] = new_issue_iid
            # upload the term's JSON
            file_name = (id_t_global +
                         '_edit_' +
                         str(edit_count)
                         )

            file_path = os.path.join(new_branch_dir,
                                     'candidate_terms',
                                     f'{file_name}.json')

            file = json.dumps(term_dict, indent=4)
            file += ' \n'
            file = json.loads(file)

            with open(file_path, 'w+') as f:
                json.dump(file, f, indent=4)

            # Stage the new files, and commit
            new_branch_repo.git.add('.')
            new_branch_repo.git.commit('-m', commit_message,
                                       author=f"{user['name']} <{user['email']}>")

            # 4. Reopen all of the term's closed issues, and remove the 'approved' tag
            all_closed_issues = api.get_issues_list(params={'state': 'closed',
                                                            'order_by': 'created_at',
                                                            'sort': 'desc',
                                                            'per_page': 100})
            relevant_closed_issues = [x for x in all_closed_issues
                                      if id_t_global in x['title']]
            for x in relevant_closed_issues:
                params = {'state_event': 'reopen'}
                if 'approved' in x['labels']:
                    params['remove_labels'] = 'approved'
                gitlab.put(f"{ep.ISSUES_EP}/{x['iid']}",
                           params=params)
            # TODO: optional push
            new_branch_repo.git.push('-u', 'origin', new_branch_name)
            return redirect(url_for('.view_term',
                                    issue_iid=new_issue_iid,
                                    path="-"))

        else:  # Term has not been approved yet
            id_t_global_temp = form_data_dict['id_t_global']
            branches = api.get_branch_names()
            if len(branches) == 0:
                branches = api.get_branch_names()
            branches = [x for x in branches if x != 'main']  # remove 'main'
            new_term_branches = [
                x for x in branches
                if (('new_term_' in x) and
                    (id_t_global_temp in x))
                ]
            reopen_term_branches = [x for x in branches
                                    if (('term_reopening_' in x) and
                                        (id_t_global_temp in x))
                                    ]

            branch_name = (new_term_branches + reopen_term_branches)[0]
            branch_dir = os.path.join(vocabulary_branches_path,
                                      f"{vocabulary_name}-{branch_name}")

            term_file_path = os.path.join(branch_dir,
                                          'candidate_terms',
                                          term_file_name)

            with open(term_file_path) as f:
                term_file = json.load(f)

            prev_term_metadata = term_file['metadata']

            # preprocess the form data:
            pre_json_data = pre_jsonify_form(form.data)

            # generate term metadata
            term_metadata = {}
            term_metadata['created_by_user_id'] = prev_term_metadata['created_by_user_id']
            term_metadata['created_on'] = prev_term_metadata['created_on']
            term_metadata['edited_by_user_id'] = user['id']
            term_metadata['edited_on'] = (datetime
                                          .utcnow()
                                          .isoformat())
            term_metadata['id_t_global'] = (prev_term_metadata.get('id_t_global',
                                            (term_tag(pre_json_data.get('label')) +
                                             '-' + generate_uid() +
                                             '-vp'))
                                            )

            term_metadata['id_t_local'] = ""
            term_metadata['id_t_version'] = generate_uid() + '-vp'
            term_metadata['generated_from_id_t_version'] = prev_term_metadata.get('id_t_version',
                                                                                  generate_uid() + '-vp')
            term_metadata['generated_from_file_name'] = term_file_name
            # generate the term's dictionary
            term_dict = {}
            term_dict['metadata'] = term_metadata
            term_dict['term'] = pre_json_data
            # if record moved under another term, remove contexual type
            if term_dict['term']['contextual_type'] != '' and len(term_dict['term']['broader']) != 0:
                term_dict['term']['contextual_type'] = ''

            id_t_global = term_dict['metadata']['id_t_global']
            if keep_old_images:
                term_dict['image_files'] = term_file['image_files']
                valid_img_upload_urls = term_dict['image_files']

            # Upload images (if such exist), and update JSON
            if not keep_old_images:
                # get the number of previous image files for this term
                branch_img_dir = os.path.join(branch_dir, 'images')
                branch_images = os.listdir(branch_img_dir)
                branch_images = [x for x in branch_images
                                 if x != '.gitkeep']
                prev_imgs = [re.split("\_[0-9]+.*", x)[0] for x in branch_images]
                prev_imgs = [x for x in prev_imgs if id_t_global in x]
                n_prev_imgs = len(prev_imgs)

                # Upload images (if such exist), and update JSON
                img_files = [x for x in form.images.data if isinstance(x, FileStorage)]
                valid_img_upload_urls = []
                base_url = session.get('BASE_URL')
                for i, img in enumerate(img_files):
                    if any(img.filename.lower().endswith(x) for x in img_filetypes):
                        img_file_name = f"{id_t_global}_{i+n_prev_imgs}"
                        _, ext = os.path.splitext(img.filename.lower())
                        img_path = f"images/{img_file_name}{ext}"
                        img.save(os.path.join(branch_dir,
                                              img_path))
                        img_url = f"{base_url}/-/raw/{branch_name}/images/{img_file_name}{ext}"
                        valid_img_upload_urls.append(img_url)

                # update the term JSON with the image file names
                term_dict['image_files'] = [x for x in valid_img_upload_urls]

            # get the consecutive number of the edit
            candidate_files = os.listdir(os.path.join(branch_dir,
                                                      'candidate_terms')
                                         )
            candidate_files = [x for x in candidate_files
                               if id_t_global in x]
            candidate_files = [x for x in candidate_files
                               if x.endswith('.json')]
            edit_count = len([x for x in candidate_files
                              if '_edit_' in x])
            edit_count += 1

            term_label = pre_json_data['label']

            commit_message = (f"Term edit #{edit_count}: {term_label}. "
                              f"Global Term ID: {id_t_global}")
            # Create the issue
            term_markdown = new_term_to_markdown(term_dict)
            new_issues_response = api.post_new_term_issue(
                title=f"[Term Edit #{edit_count}] {term_label} | {id_t_global}",
                description=term_markdown,
                labels="term_edit")
            new_issue_iid = new_issues_response.json()['iid']
            new_issue_iid = int(new_issue_iid)

            term_dict['metadata']['issue_iid'] = new_issue_iid

            # upload the term's JSON
            file = json.dumps(term_dict, indent=4)
            file += ' \n'
            file = json.loads(file)

            file_name = (id_t_global +
                         '_edit_' +
                         str(edit_count)
                         )

            file_path = os.path.join(branch_dir,
                                     'candidate_terms',
                                     f'{file_name}.json')

            with open(file_path, 'w+') as f:
                json.dump(file, f, indent=4)

            # Stage the new files, and commit
            term_edit_repo = Repo(branch_dir)
            term_edit_repo.git.add('.')
            term_edit_repo.git.commit('-m', commit_message,
                                      author=f"{user['name']} <{user['email']}>")
            # TODO: optional push
            term_edit_repo.git.push('-u', 'origin', branch_name)

            return redirect(url_for('.view_term',
                                    issue_iid=new_issue_iid,
                                    path="-"))

    return render_template('edit_term.html',
                           form=form,
                           issue_iid=int(issue_iid),
                           edit_term=form_data_dict['label'],
                           tree_dict=tree_dict)


@main.route('/view_term/<int:issue_iid>/<path>', methods=['GET', 'POST'])
@login_required
def view_term(issue_iid, path):
    ep = Endpoints()
    issue = gitlab.get(f"{ep.ISSUES_EP}/{issue_iid}")
    if not issue.ok:
        return redirect(url_for('.list_terms'))

    issue_title = issue.json()['title']
    id_t_global = issue_title.split(' | ')[1]
    all_term_issues = api.get_issues_list(
        params={'search': id_t_global,  # only issues containing id_t_global
                'order_by': 'created_at',
                'sort': 'desc',
                'per_page': 100
                })
    all_term_issues = [x for x in all_term_issues
                       if id_t_global in x['title']]
    all_term_issues = [(x['iid'], x['title'].split(' | ')[0], x['labels']) for x in all_term_issues]

    # get the status of the term version
    # check if any version has been approved
    status = 'not approved'
    if any(['approved' in x[2] for x in all_term_issues]):
        if 'approved' not in issue.json()['labels']:
            status = 'deprecated'
        else:
            status = 'approved'
    else:  # differentiate between 'not approved', and 'deprecated'
        if any(['term_reopening' in x[2] for x in all_term_issues]):
            iid_reopen = [x[0] for x in all_term_issues if 'term_reopening' in x[2]][0]  # only take the latest reopen
            if issue.json()['iid'] >= iid_reopen:
                status = 'not approved'
            else:
                status = 'deprecated'

    comments_json = []
    # reverse
    for x in reversed(all_term_issues):
        issue_comment = gitlab.get(f"{ep.ISSUES_EP}/{x[0]}"
                                   f"/notes?per_page=100&sort=asc&order_by=updated_at")
        issue_comment_json = issue_comment.json()
        for comment in issue_comment_json:
            comment['issue_name'] = x[1]
        comments_json.extend(issue_comment_json)
    term_definition = issue.json()['description']
    term_definition = markdown_to_dict(term_definition=term_definition)
    term_definition.pop('label')
    term_definition.pop('broader_labels')
    term_definition.pop('related_labels')
    term_definition = {k if k != 'broader' else 'part of': v
                       for k, v in term_definition.items()}
    term_definition = {k if k != 'related_external' else 'related (external)': v
                       for k, v in term_definition.items()}
    term_definition['data type'] = term_definition.pop('datatype')
    term_definition['contextual type'] = term_definition.pop('contextual_type')
    all_jsons = get_term_jsons()
    # map the global id to term name
    term_definition['part of'] = [term['label'] for term in all_jsons if term['id_t_global'] in term_definition['part of']]
    term_definition['related'] = [term['label'] for term in all_jsons if term['id_t_global'] in term_definition['related']]

    comments_json = [x for x in comments_json if x['system'] is False]
    data = {}
    data['title'] = issue.json()['title'].split(' | ')[0]
    data['iid'] = issue.json()['iid']
    data['approved'] = 'approved' in issue.json()['labels']
    data['status'] = status
    data['definition'] = term_definition
    
    time_diff = timedelta(hours=int(str(datetime.astimezone(datetime.now()))[-6:-3]))
    for comment in comments_json:
        t = time.strptime(comment['updated_at'].split('.')[0], '%Y-%m-%dT%H:%M:%S')
        t = datetime(t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec) 
        t = t + time_diff
        comment['updated_at'] = t.isoformat() + '.' + comment['updated_at'].split('.')[1]
    data['comments'] = comments_json

    # load comments section
    comments_form = AddNewCommentForm()
    if comments_form.validate_on_submit():
        comment = comments_form.comment.data
        gitlab.post(f"{ep.ISSUES_EP}/{issue_iid}/notes?body={comment}")
        return redirect(url_for('.view_term', issue_iid=issue_iid, path=path))

    username = gitlab.get('user').json()['username']
    awards = gitlab.get(f"{ep.ISSUES_EP}/{issue_iid}"
                        f"/award_emoji")
    thumbsup = [x for x in awards.json() if x['name'] == 'thumbsup']
    thumbsdown = [x for x in awards.json() if x['name'] == 'thumbsdown']
    awards = {'thumbsup': {}, 'thumbsdown': {}}
    awards['thumbsup']['count'] = len(thumbsup)
    awards['thumbsup']['has_voted'] = any(
        [username == x['user']['username'] for x in thumbsup]
        )
    awards['thumbsup']['awards_id'] = [x['id']
                                       for x in thumbsup
                                       if username == x['user']['username']]

    awards['thumbsdown']['count'] = len(thumbsdown)
    awards['thumbsdown']['has_voted'] = any(
        [username == x['user']['username'] for x in thumbsdown]
        )
    awards['thumbsdown']['awards_id'] = [x['id']
                                         for x in thumbsdown
                                         if username == x['user']['username']]

    # check if a vote has been sent TODO: replace with javascript
    if request.args.get('vote_intent'):
        award, intent = request.args.get('vote_intent').split('_')

        if intent == 'on':
            gitlab.post(f"{ep.ISSUES_EP}/{issue_iid}"
                        f"/award_emoji?name={award}")
        elif intent == 'off':
            gitlab.delete(f"{ep.ISSUES_EP}/{issue_iid}"
                          f"/award_emoji"
                          f"/{awards[award]['awards_id'][0]}")
        elif intent == 'switch':
            gitlab.post(f"{ep.ISSUES_EP}/{issue_iid}"
                        f"/award_emoji?name={award}")
            opposite = 'thumbsup'
            if award == 'thumbsup':
                opposite = 'thumbsdown'
            gitlab.delete(f"{ep.ISSUES_EP}/{issue_iid}"
                          f"/award_emoji"
                          f"/{awards[opposite]['awards_id'][0]}")

        return redirect(url_for('.view_term', issue_iid=issue_iid, path=path))
    # Speed-up
    tree_dict = session.get(f"tree_dict_{session.get('PROJECT_NAME', 'nan')}")
    # tree_dict = _get_tree()
    # set read cookie
    if f'visited_{issue_iid}_{session["PROJECT_NAME"]}' not in session:
        session[f'visited_{issue_iid}_{session["PROJECT_NAME"]}'] = True

    return render_template('view_term.html',
                           data=data,
                           awards=awards,
                           comments_form=comments_form,
                           all_term_issues=all_term_issues,
                           tree_dict=tree_dict,
                           dtype_color_map=dtype_color_map,
                           dtype_icon_map=dtype_icon_map)

@main.route('/authorization_status', methods=['GET'])
def authorization_status():
    """
    Checks the user's gitlab authorization status.
    """
    return Response(json.dumps({'authorized': gitlab.authorized}),
                            content_type='application/json')

@main.route('/tree_dict_status', methods=['GET'])
@login_required
def tree_dict_status():
    """
    Checks the status of the celery task responsible
    for loading the vocabulary tree. Returns a Response
    object which can be read on the front-end using
    JavaScript's fetch() API.
    """
    if session.get(f"tree_dict_task_id_{session.get('PROJECT_NAME', 'nan')}"):
        result = celery.AsyncResult(session.get(f"tree_dict_task_id_{session.get('PROJECT_NAME', 'nan')}"))
        ready = result.ready()
        if ready:
            _ = session.pop(f"tree_dict_task_id_{session.get('PROJECT_NAME', 'nan')}")
            new_tree = result.get()

            if str(new_tree) != str(session[f"tree_dict_{session.get('PROJECT_NAME', 'nan')}"]):
                session[f"tree_dict_{session.get('PROJECT_NAME', 'nan')}"] = new_tree
                return Response(json.dumps({'done': ready}),
                                content_type='application/json')
            else:
                return Response(json.dumps({'done': 'halt'}),
                                content_type='application/json')
        else:
            return Response(json.dumps({'done': False}),
                            content_type='application/json')
    else:
        return Response(json.dumps({'done': 'halt'}),
                        content_type='application/json')


def _get_tree():
    """
    A wrapper function which initiates the celery task
    used to load the vocabualry tree
    """
    ep = Endpoints()
    issues_ep = ep.ISSUES_EP
    files_ep = ep.REPO_FILES_EP

    if (session.get(f"tree_dict_{session.get('PROJECT_NAME', 'nan')}") and
            not session.get(f"tree_dict_task_id_{session.get('PROJECT_NAME', 'nan')}")):
        tree_dict_task = get_vocab_tree.delay(issues_ep, files_ep,
                                              project_id=session.get('PROJECT_ID'),
                                              project_name=session.get('PROJECT_NAME'))
        session[f"tree_dict_task_id_{session.get('PROJECT_NAME', 'nan')}"] = tree_dict_task.id
        tree_dict = session.get(f"tree_dict_{session.get('PROJECT_NAME', 'nan')}")
    else:
        tree_dict_task = get_vocab_tree.delay(issues_ep, files_ep,
                                              project_id=session.get('PROJECT_ID'),
                                              project_name=session.get('PROJECT_NAME'))
        session[f"tree_dict_{session.get('PROJECT_NAME', 'nan')}"] = tree_dict_task.get()
        tree_dict = session.get(f"tree_dict_{session.get('PROJECT_NAME', 'nan')}")
    return tree_dict


@main.route('/list_terms', methods=['GET', 'POST'])
@login_required
def list_terms():
    """
    The view function used to visualize the structure of
    the vocabulary in the 'List terms' view.
    """

    tree_dict = _get_tree()
    return render_template('list_terms.html',
                           project_name=session['PROJECT_NAME'],
                           tree_dict=tree_dict,
                           dtype_color_map=dtype_color_map,
                           dtype_icon_map=dtype_icon_map)


@main.route('/vocabulary_tree', methods=['GET'])
@login_required
def vocabulary_tree():
    """
    A function used to pass the data needed for
    the graph visualization of the vocabulary
    to the respective html template
    """
    all_jsons = get_term_jsons()

    # get all top-level terms of the vocabulary
    top_level_terms = [term_dict
                       for term_dict in all_jsons
                       if not (term_dict['broader'])]
    # generate the vocabulary tree
    tree_json = generate_tree(vocabulary_terms=all_jsons,
                              children_list=[[x['id_t_global']
                                              for x in top_level_terms]],
                              parents_list=[session.get('PROJECT_NAME',
                                                        'Vocabulary')])
    return render_template('vocabulary_tree.html', tree_json=tree_json, dtype_icon=dtype_icon_map)


@main.route('/download_json', methods=['POST'])
@login_required
def download_json():
    """
    A function used to download the JSON file
    describing the whole vocabulary.
    """
    all_jsons = get_term_jsons()
    # get all top-level terms of the vocabulary
    top_level_terms = [term_dict for term_dict in all_jsons
                       if not (term_dict['broader'])]

    # generate the vocabulary tree
    tree_json = generate_tree(vocabulary_terms=all_jsons,
                              children_list=[[x['id_t_global'] for x in top_level_terms]],
                              parents_list=[session.get('PROJECT_NAME',
                                                        'Vocabulary')])

    # metadata about the download Response object
    filename = f"{session.get('PROJECT_NAME', 'Vocabulary').replace(' ', '_')}.json"
    mimetype = 'application/json'
    headers = {
        'Content-Disposition': f'attachment;filename={filename}'
        }

    # the returned Response initiates a file download
    return Response(tree_json,
                    mimetype=mimetype,
                    headers=headers)


@main.route('/import_vocabulary', methods=['GET', 'POST'])
@login_required
def import_vocabulary():
    """
    A function used to upload the JSON files
    of multiple terms, from which a new vocabulary
    is generated.
    """
    form = TermsJSONsUploadForm()

    if form.validate_on_submit():
        # create a new repository, and select it for
        # use by VocPopuli

        repository_name = form.vocab_name.data
        repository_description = (f"This is the repository of the {repository_name} vocabulary.  \n"
                                  f" \n "
                                  f"The vocabulary was created using [https://vocpopuli.com](https://vocpopuli.com).  \n")

        if repository_name:
            repository_response = api.create_new_vocab_repository(
                repository_name,
                repository_description
                )
            if repository_response.get('id', False):
                # Update all of the projects the user
                # has access to
                user_projects = gitlab.get('projects',
                                           params={'membership': True})
                # Update the projects-related session cookie
                vocab_projects = {}
                for repo in user_projects.json():
                    repo_id = repo['id']
                    repo_tree = gitlab.get(f'projects/{repo_id}/repository/tree')
                    top_level_paths = [x['path']
                                       for x in repo_tree.json()]
                    if ('approved_terms' in top_level_paths
                            and 'candidate_terms' in top_level_paths):
                        vocab_projects[repo['name']] = repo_id

                session['USER_PROJECTS'] = [x['name']
                                            for x in user_projects.json()]
                session['PROJECTS_DICT'] = vocab_projects
                # store the new repository information in the session cookie
                session['PROJECT_ID'] = repository_response['id']
                session['PROJECT_NAME'] = repository_response['name']
                session['PROJECT_SET'] = True

            else:  # TODO: flash an error message
                return redirect(url_for('.import_vocabulary'))
        ep = Endpoints()
        all_repository_jsons = get_term_jsons()
        all_repository_labels = [x['label'] for x in all_repository_jsons]

        # store the terms' JSON dicts in memory
        terms_json_dicts = []
        for file in form.files.data:
            if file.filename.lower().endswith('.json'):
                json_str = file.read()
                json_dict = json.loads(json_str)
                terms_json_dicts.append(json_dict)

        all_terms_labels = [x['term']['label'] for x in terms_json_dicts]
        user = gitlab.get('user').json()
        for x in terms_json_dicts:
            # If the term is already in the repository -> disregard it
            if x['term']['label'] in all_repository_labels:
                continue

            # validate whether all term relations point to existing terms
            all_related_terms = x['term']['broader'] + x['term']['related']
            for t in all_related_terms:
                if t not in all_terms_labels:
                    print(f"Term {x['term']['label']} has a relation"
                          f"to term {t} which is not in the"
                          f"proposed vocabulary")  # TODO: flash an error

            now = (datetime.utcnow().isoformat())
            x['metadata'] = {
                'created_by_user_id': user['id'],
                'created_on': now,
                'edited_by_user_id': user['id'],
                'edited_on': now,
                'id_t_global': (term_tag(x['term']['label']) +
                                '-' + generate_uid() +
                                '-vp'),
                'id_t_local': generate_uid() + '-vp',
                'id_t_version': generate_uid() + '-vp'
            }
            x['image_files'] = []
            x['term']['broader_labels'] = x['term']['broader']
            x['term']['broader'] = []
            x['term']['related_labels'] = x['term']['related']
            x['term']['related'] = []
            if 'related_external' not in x['term'].keys():
                x['term']['related_external'] = [""]
            if 'contextual_type' not in x['term'].keys():
                x['term']['contextual_type'] = None

        for x in terms_json_dicts:
            if x['term']['label'] in all_repository_labels:
                continue
            for broader_label in x['term']['broader_labels']:
                broader_id = [n['metadata']['id_t_global']
                              for n in terms_json_dicts
                              if n['term']['label'] == broader_label][0]
                x['term']['broader'].append(broader_id)

            for related_label in x['term']['related_labels']:
                related_id = [n['metadata']['id_t_global']
                              for n in terms_json_dicts
                              if n['term']['label'] == related_label][0]
                x['term']['related'].append(related_id)

            file = json.dumps(x, indent=4)
            file += ' \n'  # add a new line at the end of the file

            id_t_global = x['metadata']['id_t_global']
            term_label = x['term']['label']
            file_name = id_t_global
            # TODO: differentiate between approved/not approved terms
            # use URL-encoding for '/' and '.' when defining
            # the term's file path
            file_path = f"approved_terms%2F{file_name}%2Ejson"

            commit_message = (f"VocPopuli import: {term_label}. "
                              f"Global Term ID: {id_t_global}")

            # commit the file in the main branch

            commit_response = api.add_new_file(file=file,
                                               file_path=file_path,
                                               branch="main",
                                               commit_message=commit_message)

            if commit_response:
                # generate an issue description
                term_markdown = new_term_to_markdown(x)

                # post a new issue related to the term's definition
                # try multiple times because sometimes the API returns
                # a 500 code on the first try
                issue_response = api.post_new_term_issue(
                    title=f'[New Term] {term_label} | {id_t_global}',
                    description=term_markdown,
                    labels="new_term, approved"
                    )
                if not issue_response.ok:
                    issue_response = api.post_new_term_issue(
                        title=f'[New Term] {term_label} | {id_t_global}',
                        description=term_markdown,
                        labels="new_term, approved")

                    if not issue_response.ok:
                        issue_response = api.post_new_term_issue(
                            title=f'[New Term] {term_label} | {id_t_global}',
                            description=term_markdown,
                            labels="new_term, approved")

                if issue_response.ok:
                    # close the issue because the term is approved
                    params = {'state_event': 'close'}
                    iid = issue_response.json()['iid']
                    gitlab.put(f"{ep.ISSUES_EP}/{iid}",
                               params=params)

            else:
                print(term_label, file_name)
        load_repositories()
        return redirect(url_for('.list_terms'))

    return render_template('import_vocabulary.html', form=form)


@main.route('/approve_term/<int:issue_iid>', methods=['GET'])
@login_required
@update_info
def approve_term(issue_iid):
    """
    The function used to approve a given term.

    issue_iid --- The IID of the issue linked to the version of the
    term which the user wants to edit.
    """
    ep = Endpoints()
    user = gitlab.get('user').json()
    vocabulary_name = session['PROJECT_NAME'].replace(' ', '-')
    vocabulary_branches_path = os.path.join(
            current_app.config['VOCABULARIES_DIR'],
            vocabulary_name
            )
    # get the term's issue
    issue = gitlab.get(f"{ep.ISSUES_EP}/{issue_iid}")
    # basic error handling
    if not issue.ok:
        return redirect(url_for('.view_term', issue_iid=issue_iid, path='none'))

    issue_title = issue.json()['title']
    term_dict_from_issue = markdown_to_dict(issue.json()['description'])
    id_t_global = term_dict_from_issue['id_t_global']

    # get the name of the respective term json file
    if '[New Term]' in issue_title:
        term_file_name = (id_t_global + '.json')

    elif '[Term Edit #' in issue_title:
        start_idx = issue_title.find('#') + 1
        end_idx = issue_title.find(']')
        edit_n = int(issue_title[start_idx:end_idx])

        term_file_name = (id_t_global +
                          '_edit_' +
                          str(edit_n) +
                          '.json')

    # get the current term branch
    branches = [x for x in api.get_branch_names()
                if id_t_global in x]

    # rudimentary error handling
    if len(branches) > 1:
        return redirect(url_for('.view_term', issue_iid=issue_iid, path='none'))
    elif len(branches) == 0:
        branches = [x for x in api.get_branch_names()
                    if id_t_global in x]

    term_branch_name = branches[0]

    # get all issues related to the term
    all_term_issues = api.get_issues_list(
        params={'search': id_t_global,  # only issues containing id_t_global
                'order_by': 'created_at',
                'sort': 'desc',
                'per_page': 100
                })

    all_term_issues = [x for x in all_term_issues
                       if x['title'].split(' | ')[1] == id_t_global]

    all_issues_open = all([x['state'] == 'opened' for x in all_term_issues])
    no_issue_approved = all(['approved' not in x['labels']
                             for x in all_term_issues])
    # rudimentary error handling
    if not (all_issues_open and no_issue_approved):
        return redirect(url_for('.view_term', issue_iid=issue_iid, path='none'))
    term_branch_dir = os.path.join(vocabulary_branches_path,
                                   f"{vocabulary_name}-{term_branch_name}")
    term_file_path = os.path.join(term_branch_dir,
                                  'candidate_terms',
                                  term_file_name)
    term_branch_repo = Repo(term_branch_dir)
    term_branch_repo.git.checkout(term_branch_name)
    term_branch_repo.git.pull()
    # add an id_t_local to the approved term's json
    with open(term_file_path, 'r') as f:
        term_dict = json.load(f)
    term_dict["metadata"]["id_t_local"] = generate_uid() + '-vp'
    # rudimentary error handling:
    if term_dict['metadata']['id_t_global'] != id_t_global:
        return redirect(url_for('.view_term', issue_iid=issue_iid, path='none'))
    with open(term_file_path, 'w') as f:
        json.dump(term_dict, f, indent=4)

    new_term_file_path = os.path.join(term_branch_dir,
                                      'approved_terms',
                                      f"{id_t_global}.json")

    (term_branch_repo.config_writer()
        .set_value("user", "name", user['name']).release())
    (term_branch_repo.config_writer()
        .set_value("user", "email", user['email']).release())
    move(src=term_file_path, dst=new_term_file_path)
    term_branch_repo.git.add('.')
    term_branch_repo.git.commit('-m', f"Approved {term_file_name}")
    term_branch_repo.git.push()
    term_branch_repo.git.checkout('main')
    term_branch_repo.git.pull()
    term_branch_repo.git.merge(term_branch_name)

    # empty the 'candidate_terms' folder of the 'main' branch
    candidate_terms_dir = os.path.join(term_branch_dir,
                                       'candidate_terms')
    candidate_jsons = os.listdir(candidate_terms_dir)
    n_jsons = len([x for x in candidate_jsons if x.endswith(".json")])
    if n_jsons > 0:
        for file in candidate_jsons:
            if file.endswith(".json"):
                os.remove(os.path.join(candidate_terms_dir, file))
        term_branch_repo.git.add('.')
        term_branch_repo.git.commit('-m', "Deleted the candidate terms from main")
    term_branch_repo.git.push()

    # pull the main branch directory
    main_branch_dir = os.path.join(vocabulary_branches_path,
                                   f"{vocabulary_name}-main")
    main_branch_repo = Repo(main_branch_dir)
    main_branch_repo.git.pull()

    # close the issues, and add an 'approved' tag
    for x in all_term_issues:

        if x['iid'] == issue_iid:
            desc = x['description']
            desc = desc.replace("Local Term ID:</strong> ",
                                f"Local Term ID:</strong> {term_dict['metadata']['id_t_local']}")
            params = {'state_event': 'close',
                      'add_labels': 'approved',
                      'description': desc}
            resp = gitlab.put(f"{ep.ISSUES_EP}/{x['iid']}",
                              params=params)
        else:
            params = {'state_event': 'close'}
            resp = gitlab.put(f"{ep.ISSUES_EP}/{x['iid']}",
                              params=params)
        if not resp.ok:
            resp = gitlab.put(f"{ep.ISSUES_EP}/{x['iid']}",
                              params=params)

    return redirect(url_for('.list_terms'))


@main.route('/kadi_export', methods=['GET', 'POST'])
@login_required
def kadi_export():
    all_jsons = get_term_jsons()
    # get all top-level terms of the vocabulary
    top_level_terms = [term_dict for term_dict in all_jsons
                       if not (term_dict['broader'])]

    # generate the vocabulary tree
    tree_json = generate_tree(vocabulary_terms=all_jsons,
                              children_list=[[x['id_t_global'] for x in top_level_terms]],
                              parents_list=[session.get('PROJECT_NAME',
                                                        'Vocabulary')])

    all_vocabularies_dict = json.loads(tree_json)
    possible_vocabs = []
    for term in all_vocabularies_dict['children']:
        if term['datatype'] == 'record':
            possible_vocabs.append(term['name'])

    form = KadiTemplateExport(possible_templates=possible_vocabs)

    if form.validate_on_submit():
        chosen_template = form.template.data
        for term in all_vocabularies_dict['children']:
            if term['name'] == chosen_template:
                chosen_vocab = term
        
        # checks if the title starts with the name of the chosen record
        title_pattern = '^' + chosen_template + '.*'
        if not re.search(title_pattern, form.title_template.data):
            form.title_template.data = chosen_template + ' ' + form.title_template.data
            return render_template('kadi_export.html', form=form, error="Title has to start with name of record.")

        # checks if the title starts with the name of the chosen record
        title_pattern = '^' + chosen_template + '.*'
        if not re.search(title_pattern, form.title_template.data):
            form.title_template.data = chosen_template + ' ' + form.title_template.data
            return render_template('kadi_export.html', form=form, error="Title has to start with name of record.")

        datatype_conversion = {
            'dictionary': 'dict',
            'string': 'str',
            'integer': 'int',
            'boolean': 'bool'
        }

        def read_layer(searched_part):
            # list containing dictionaries: each dictionary read in as a layer
            # and returned as a whole as a list
            if type(searched_part) == list:
                list_terms = []
                for term in searched_part:
                    list_terms.append(read_layer(term))
                return list_terms
            # layer is a key
            elif "name" in searched_part and "datatype" in searched_part:
                # term is not a key but an option
                if searched_part["datatype"] in ["individual", "option"]:
                    return searched_part["name"]
                key = to_key(searched_part)
                if "children" in searched_part:
                    values = read_layer(searched_part["children"])
                    if type(values) == list and len(values) > 0 and type(values[0]) == dict:
                        key["value"] = values
                    elif type(values) == list and len(values) > 0 and type(values[0]) != list:
                        key["validation"] = {"options": values} 
                    else:
                        key["value"] = values
                return key
            else:
                print('incomplete key')
                return None

        def to_key(term_dict: dict):
            key_dict = {
                    "key": term_dict["name"]}
            if term_dict.get("purl", None):
                key_dict["term"] = term_dict.get("purl")
            if term_dict["datatype"] in datatype_conversion:
                key_dict["type"] = datatype_conversion[term_dict["datatype"]]
            else:
                key_dict["type"] = term_dict["datatype"]
            
            return key_dict

        extras = []
        if "children" in chosen_vocab:
            extras = read_layer(chosen_vocab["children"])
        
        if ',' in form.tags.data:
            tags = form.tags.data.split(',')
            tags = [x.strip() for x in tags]
        elif form.tags.data == '':
            tags = []
        else:
            tags = [form.tags.data.strip()]
            
        # tags = tags + [
        #     "@id_v_global:" + all_vocabularies_dict["id_v_global"],
        #     "@id_v_local:" + all_vocabularies_dict["id_v_local"],
        #     "@id_t_global:" + chosen_vocab["id_t_global"],
        #     "@id_t_local:" + chosen_vocab["id_t_local"]
        # ]
        
        tags = tags + [
             all_vocabularies_dict["id_v_global"],
             all_vocabularies_dict["id_v_local"],
             chosen_vocab["id_t_global"],
             chosen_vocab["id_t_local"]
         ]
        
        template = {
            "description": form.description.data,
            "extras": extras,
            "license": "CC-BY-4.0",
            "tags": tags,
            "title": form.title_template.data,
            "type": chosen_vocab["datatype"]
        }

        def Template_Create(data, title, local_id, instance=None,
                            host=None, pat=None, group_id=None, description=None):

            template = KadiManager(instance=instance,
                                   host=host,
                                   token=pat).template(identifier=(local_id+str(uuid.uuid4())),
                                                       type='record',
                                                       data=data,
                                                       create=True,
                                                       title=title,
                                                       description=description)
            template.add_group_role(group_id=group_id, role_name='editor')

        if form.pat.data != "" and form.pat.data is not None:
            try:
                Template_Create(template,
                                form.title_template.data,
                                local_id=chosen_vocab["id_t_local"],
                                host=form.host.data,
                                pat=form.pat.data,
                                group_id=8,
                                description=form.description.data)
                return redirect(url_for('main.list_terms'))
            except:
                return render_template('kadi_export.html', form=form, error="Invalid PAT or Host.")
                
        # if no PAT to Kadi is given the template will be downloaded as a json
        else:
            file_name = form.title_template.data
            file_name = file_name.replace(' ', '_')
            # metadata about the download Response object
            mimetype = 'application/json'
            headers = {
                'Content-Disposition': f'attachment;filename={file_name}_template.json'
                }
            # the returned Response initiates a file download
            return Response(json.dumps(template, indent=4),
                            mimetype=mimetype,
                            headers=headers)

        #return redirect(url_for('main.list_terms'))

    return render_template('kadi_export.html', form=form)


@main.route('/public_view_term/<id_v_global>/<id_t_global>', methods=['GET'])
def public_view_term(id_v_global, id_t_global):
    # get https://gitlab.com/projects/41151548/ -> get 'location' -> use location for raw file
    id_v_global = str(id_v_global)
    id_t_global = str(id_t_global)
    # id_v_global has the form: xxxxxxx-<project_id>-vp
    project_id = id_v_global.split('-')[1]
    project_id = project_id.split('-')[0]

    issues_endpoint = f"https://gitlab.com/api/v4/projects/{project_id}/issues/"
    search_params = {'private_token': os.environ.get('VOCPOPULI_TOKEN'),
                     'search': id_t_global,  # only issues containing id_t_global
                     'order_by': 'created_at',
                     'sort': 'desc',
                     'per_page': 100
                     }
    issues_response = requests.get(f"{issues_endpoint}",
                                   params=search_params)
    issues_dict_list = issues_response.json()

    # handle pagination
    for page in range(2, 1 + int(issues_response.headers['X-Total-Pages'])):
        search_params['page'] = page
        next_page = requests.get(f"{issues_endpoint}",
                                 params=search_params)
        issues_dict_list += next_page.json()

    # double-check
    issues_dict_list = [x for x in issues_dict_list
                        if id_t_global in x['title']]

    # get approval status
    is_approved = any(['approved' in x['labels']
                       for x in issues_dict_list])

    if is_approved:
        issue = [x for x in issues_dict_list
                 if 'approved' in x['labels']][0]
    else:
        issue = issues_dict_list[0] # get latest issue

    term_definition = issue['description']
    term_definition = markdown_to_dict(term_definition=term_definition)
    term_definition.pop('label')
    term_definition.pop('broader_labels')
    term_definition.pop('related_labels')
    term_definition = {k if k != 'broader' else 'Part of': v
                       for k, v in term_definition.items()}
    term_definition = {k if k != 'related_external' else 'Related (external)': v
                       for k, v in term_definition.items()}
    term_definition['data type'] = term_definition.pop('datatype')
    term_definition['contextual type'] = term_definition.pop('contextual_type')

    data = {}
    data['title'] = issue['title'].split(' | ')[0]
    data['iid'] = issue['iid']
    data['definition'] = term_definition

    return render_template('public_view_term.html',
                           data=data,
                           dtype_color_map=dtype_color_map,
                           dtype_icon_map=dtype_icon_map)


@main.route('/publish_vocabulary', methods=['GET', 'POST'])
@login_required
def publish_vocabulary():

    namespaces_resp = gitlab.get('namespaces')
    namespaces_json = namespaces_resp.json()
    namespaces = [(x['id'], x['name']) for x in namespaces_json]

    form = PublishVocabularyForm(namespaces=namespaces)

    if form.validate_on_submit():
        ep = Endpoints()

        # Format the vocabulary tag
        vocab_tag = form.vocabulary_tag.data.lower()
        vocab_tag = ''.join(x for x in vocab_tag if x.isalnum())

        if len(vocab_tag) >= 7:
            vocab_tag = vocab_tag[:7]
        else:
            rest = 7 - len(vocab_tag)
            vocab_tag = vocab_tag + 'x'*rest

        # Get all vocabulary terms' JSONs
        all_jsons = get_term_jsons()
        # Generate the Vocabulary IDs
        id_t_locals = [x.get('id_t_local', '') for x in all_jsons if x['approved']]
        id_t_locals = sorted([x for x in id_t_locals if x != ""])
        id_t_locals_str = ', '.join(x for x in id_t_locals)
        n_approved_str = f'{len(id_t_locals):06}'

        id_t_versions = sorted([x.get('id_t_version', '') for x in all_jsons])
        id_t_versions_str = ', '.join(x for x in id_t_versions)

        if id_t_locals_str != '':
            id_v_local = (n_approved_str + '-' +
                          (hashlib.md5(id_t_locals_str.encode('utf-8')).hexdigest()) +
                          '-vp')
        else:
            id_v_local = (n_approved_str + '-' + 'n/a' + '-vp')

        id_v_version = ((hashlib.md5(id_t_versions_str.encode('utf-8'))
                        .hexdigest()) + '-vp')
        id_v_global = (vocab_tag + '-' +
                       str(session.get('PROJECT_ID', 'n/a')) +
                       '-vp')

        desc = (f"This is the published version of the {session.get('PROJECT_NAME', 'n/a')} vocabulary.  \n"
                f"The file `metadata.json` contains the vocabulary's metadata as per VocPopuli's schema (in progress).  \n"
                f" \n "
                f"The vocabulary was created using [https://vocpopuli.com](https://vocpopuli.com).  \n")

        namespace_id = form.namespace.data
        serializer_namespace = "https://purls.helmholtz-metadaten.de/vp/"

        # Try to publish the vocabulary in the specified namespace
        publish_response = gitlab.post(
            'projects',
            data={
                "name": f"Vocabulary {session.get('PROJECT_NAME', 'n/a')}",
                "namespace_id": namespace_id,
                "description": desc,
                "initialize_with_readme": False,
                "default_branch": "main",
                "remove_source_branch_after_merge": False,
                "visibility": "public"
                }
            )


        if publish_response.json().get('message', {}).get('path', ['na'])[0] == 'has already been taken':
            user_repos = gitlab.get('projects', params={'membership': True}).json()
            repos_in_ns = [x for x in user_repos
                           if int(x['namespace']['id']) == int(namespace_id)]
            repo = [x for x in repos_in_ns 
                    if x['name'] == f"Vocabulary {session.get('PROJECT_NAME', 'n/a')}"][0]
            repo_id = repo['id']
            created_at = repo['created_at']
            new_repo = False
            web_url = repo['web_url']
        else:
            repo_id = publish_response.json()['id']
            created_at = publish_response.json()['created_at']
            new_repo = True
            web_url = publish_response.json()['web_url']

        # Generate vocabulary metadata:
        repo_info = gitlab.get(ep.PROJECT_EP, params={'license': True}).json()
        publisher = gitlab.get('user').json()['name']
        license = repo_info['license']
        contributors = gitlab.get(f"{ep.REPO_EP}/contributors").json()

        metadata_dict = {"VOCABULARY_NAME": session.get('PROJECT_NAME', 'n/a'),
                         "VOCABULARY_PURL": f"{serializer_namespace}{id_v_global}",
                         "VOCPOPULI_GLOBAL_ID": id_v_global,
                         "VOCPOPULI_LOCAL_ID": id_v_local,
                         "VOCPOPULI_VERSION_ID": id_v_version,
                         "VOCABULARY_PUBLISHER": publisher,
                         "VOCABULARY_PUBLISHED_ON_UTC": created_at,
                         "VOCABULARY_UPDATED_ON_UTC": datetime.utcnow().isoformat(),
                         "VOCABULARY_LICENSE": license,
                         "VOCABUALRY_CONTRIBUTORS": contributors}

        metadata_str = json.dumps(metadata_dict, indent=4)
        # Serialize the vocabulary using JSON-LD
        serializer = SKOSSerializer(namespace=serializer_namespace,
                                    term_jsons=all_jsons,
                                    id_v_global=id_v_global,
                                    vocab_name=session.get('PROJECT_NAME', None),
                                    publisher=metadata_dict["VOCABULARY_PUBLISHER"],
                                    created=metadata_dict["VOCABULARY_PUBLISHED_ON_UTC"],
                                    license_url=metadata_dict["VOCABULARY_LICENSE"]["html_url"],
                                    contributor_names = [x['name'] for x in metadata_dict["VOCABUALRY_CONTRIBUTORS"]])

        serialization = serializer.serialize_vocabulary(format="ttl")
        jsonld_serialization = serializer.serialize_vocabulary(format="json-ld")
        # Add a description of the vocabulary

        serialization_ep = (f"projects/{repo_id}/"
                      f"repository/files/SKOS_{session.get('PROJECT_NAME', 'n/a').replace(' ', '_')}%2Ettl")
        jsonld_serialization_ep = (f"projects/{repo_id}/"
                      f"repository/files/SKOS_{session.get('PROJECT_NAME', 'n/a').replace(' ', '_')}%2Ejsonld")
        readme_ep = (f"projects/{repo_id}/"
                     f"repository/files/README%2Emd")
        license_ep = (f"projects/{repo_id}/"
                      f"repository/files/LICENSE")
        metadata_ep = (f"projects/{repo_id}/"
                       f"repository/files/metadata%2Ejson")

        term_update_lists = []

        if new_repo:
            # Single term serializations:
            for x in all_jsons:
                term_serializazion = SKOSSerializer(term_jsons=[x],
                                                    id_v_global=id_v_global).serialize_term(format='ttl')
                term_ep = (f"projects/{repo_id}/"
                           f"repository/files/terms%2F{x['id_t_global']}%2Ettl")
                term_post_resp = gitlab.post(term_ep,
                    data={"branch": "main",
                        "content": term_serializazion,
                        "commit_message": f"Add {x['id_t_global']}.ttl"}
                    )
                if term_post_resp.ok:
                    term_update_lists.append({'issue_iid': x['issue_iid'],
                                              'purl': f"{serializer.namespace}{id_v_global}/{x['id_t_global']}",
                                              'gitlab_raw_url':f"{web_url}/-/raw/main/terms/{x['id_t_global']}.ttl"})

            gitlab.post(
                readme_ep,
                data={"branch": "main",
                    "content": desc,
                    "commit_message": f"Add README.md"}
            )
            gitlab.post(
                serialization_ep,
                data={"branch": "main",
                    "content": serialization,
                    "commit_message": f"Add SKOS_{session.get('PROJECT_NAME', 'n/a').replace(' ', '_')}.ttl"}
            )
            gitlab.post(
                jsonld_serialization_ep,
                data={"branch": "main",
                    "content": jsonld_serialization,
                    "commit_message": f"Add SKOS_{session.get('PROJECT_NAME', 'n/a').replace(' ', '_')}.jsonld"}
            )
            gitlab.post(
                metadata_ep,
                data={"branch": "main",
                    "content": metadata_str,
                    "commit_message": "Add metadata.json"}
            )
            gitlab.post(
                license_ep,
                data={"branch": "main",
                    "content": CC_BY_40,
                    "commit_message": "Add LICENSE"}
            )
            action = 'created'
        else:
            commit_message = f"Update the {session.get('PROJECT_NAME', 'n/a')} vocabulary."
            # Single term serializations:
            for x in all_jsons:
                term_serializazion = SKOSSerializer(term_jsons=[x],
                                                    id_v_global=id_v_global).serialize_term(format='ttl')
                term_ep = (f"projects/{repo_id}/"
                           f"repository/files/terms%2F{x['id_t_global']}%2Ettl")
                try:
                    term_post_resp = gitlab.post(term_ep,
                        data={"branch": "main",
                            "content": term_serializazion,
                            "commit_message": commit_message}
                        )
                except:
                    term_post_resp = gitlab.put(term_ep,
                        data={"branch": "main",
                            "content": term_serializazion,
                            "commit_message": commit_message}
                        )
                term_update_lists.append({'issue_iid': x['issue_iid'],
                                            'purl': f"{serializer.namespace}{id_v_global}/{x['id_t_global']}",
                                            'gitlab_raw_url':f"{web_url}/-/raw/main/terms/{x['id_t_global']}.ttl"})
                    
            gitlab.put(
                readme_ep,
                data={"branch": "main",
                    "content": desc,
                    "commit_message": commit_message}
            )
            gitlab.put(
                serialization_ep,
                data={"branch": "main",
                    "content": serialization,
                    "commit_message": commit_message}
            )
            gitlab.put(
                jsonld_serialization_ep,
                data={"branch": "main",
                    "content": jsonld_serialization,
                    "commit_message": commit_message}
            )
            gitlab.put(
                metadata_ep,
                data={"branch": "main",
                    "content": metadata_str,
                    "commit_message": commit_message}
            )

            action='updated'
        _update_issues(term_update_lists)
        vocab_url = web_url
        return render_template('publish_success.html',
                               vocab_url=vocab_url,
                               action=action)

    return render_template('publish_vocabulary.html', form=form)


def _update_issues(term_update_list):
    ep = Endpoints()
    for term in term_update_list:
        issue_iid = term['issue_iid']
        purl = term['purl']
        gitlab_raw_url = term['gitlab_raw_url']

        issue = gitlab.get(f"{ep.ISSUES_EP}/{issue_iid}").json()
        issue_desc = issue['description']
        if "<p><strong>PURL:</strong> " not in issue_desc:
            issue_desc += f"<p><strong>PURL:</strong> {purl}</p>"
        else:
            purl_start_idx = (issue_desc.find('<p><strong>PURL:</strong> ')
                       + len('<p><strong>PURL:</strong> '))
            purl_end_idx = issue_desc.find('</p>', purl_start_idx)
            purl_old = issue_desc[purl_start_idx:purl_end_idx]
            issue_desc.replace(purl_old, purl)

        if "<p><strong>GitLab SKOS URL:</strong> " not in issue_desc:
            issue_desc += f"<p><strong>GitLab SKOS URL:</strong> {gitlab_raw_url}</p>"
        else:
            url_start_idx = (issue_desc.find('<p><strong>GitLab SKOS URL:</strong> ')
                       + len('<p><strong>GitLab SKOS URL:</strong> '))
            url_end_idx = issue_desc.find('</p>', url_start_idx)
            url_old = issue_desc[url_start_idx:url_end_idx]
            issue_desc = issue_desc.replace(url_old, gitlab_raw_url)

        resp = gitlab.put(f"{ep.ISSUES_EP}/{issue_iid}",
                          params={'description': issue_desc})
        if not resp.ok:
            resp = gitlab.put(f"{ep.ISSUES_EP}/{issue_iid}",
                          params={'description': issue_desc})
        if not resp.ok:
            resp = gitlab.put(f"{ep.ISSUES_EP}/{issue_iid}",
                          params={'description': issue_desc})



@main.route('/request_vocabulary', methods=['GET'])
def request_vocabulary():
    return render_template('request_vocabulary.html')