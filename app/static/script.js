var url = window.location.href;
var pathname = window.location.pathname;
// navbar initializations
$(function () {
    // get the full URL at the address bar
    $(".nav-link").each(function () {
    // checks if its the same on the address bar
    if (url == (this.href)) {
        $(this).addClass("active");
    }
    });
});
// dragable sidebar
var dragging = false;
$('.dragholder').mousedown(function(e){
  e.preventDefault();
  dragging = true;
  $(document).mousemove(function(e){
  if (e.pageX < window.innerWidth/2 && e.pageX > window.innerWidth/10) {
  $('body').css("--left-width", e.pageX+"px");
  }
  });
});
$(document).mouseup(function(e){
  if (dragging) {
    $(document).unbind('mousemove');
    dragging = false;
  }
});
// use class name to get hyper reference from tree list
function getURL(className) {
    // replace space with - to get the class name
    var issue = $('li.'+className.replace(/ /g, "-")).children('div').children('#term-name').attr('href');
    var issueID = issue.split('/').slice(-2).reverse().pop();
    return '/view_term/' + issueID + '/' + encodeURI(className);
}
function showParents(current) {
    if (current.parent('span').length > 0) {
        current.addClass('selected-term');
        current.parent('span').parent('div').parent('li').parents('li').each(function () {
            $(this).children('.collapse').collapse('show');
        })
    } else {
        current.parent('div').addClass('selected-item');
        current.parent('div').parent('li').parents('li').each(function () {
            $(this).children('.collapse').collapse('show');
        })
    }
}

function markParents(current) {
    if (current.parent('span').length == 0) {
        current.parent('div').parent('div').parent('li').parents('li').addClass('child-searched');
    }
}
function removeMarks() {
    $('.child-searched').removeClass('child-searched');
    $('.searched').removeClass('searched');
}
function collapseNotSearched() {
    $('li').each(function() {
        if(!$(this).hasClass('child-searched')) {
            $(this).children('.collapse').collapse('hide');
        }
    })
}
function showMarked() {
    $('.child-searched').children('.collapse').collapse('show');
}

function collapseAll() {
    $('.collapse').collapse('hide');
}
// search for tree list
$(".list-filter").on("keyup", function() {
    if ($(this).val() == '') {
        collapseAll();
        removeMarks();
    } else {
        var termName = $(this).val().toLowerCase();
        removeMarks();
        $("#tree2 #term-name").each(function() {
            if($(this).text().toLowerCase().indexOf(termName) > -1) {
                $(this).addClass('searched');
                markParents($(this));
            };
        });
        collapseNotSearched();
        showMarked();
    }
})

// page contents initializations
$(function () {
    if (pathname.indexOf("list_terms") >= 0) {
    } else {
        var splitUrlArray = pathname.split('/'); 
        var lastPart = splitUrlArray.pop();
        var issueID = splitUrlArray.pop();
        // open the parents on the sidebar
        if (lastPart=="-") {
            $("a[href*='"+issueID+'/-'+"']").each(function() {
                showParents($(this));
            });
        } else {
            var current = $("a[href$='"+lastPart+"']");
            $("a[href$='"+lastPart+"']").each(function() {
                showParents($(this));
            });
        }
        // view term page
        if (pathname.indexOf("view_term") >= 0) {
            $('#tree1>li>div>#term-name').addClass("parent");
            // breadcrumb
            var splitPathArray = decodeURI(lastPart).split('-');
            splitPathArray = splitPathArray.filter(item => item);
            for(var i = 0; i < splitPathArray.length - 1; i++) {
                $('<li class="breadcrumb-item"><a href="' + getURL(splitPathArray[i]) + '">' + splitPathArray[i] + '</a></li>').insertBefore('ol.breadcrumb li.active');
              }
            // keep the term version after selecting version on the view term page
            $('#term-version option').attr("value", function() { return $(this).attr("value") + lastPart });
            $("#term-version").val(pathname);
            $('.term-version-item').attr("href", function() { return $(this).attr("href") + lastPart });
            var version = $("a[href*='"+issueID+'/-'+"']").text();
            $('.version-item').text(version)
            $('.edit-button').attr("href", function() { return $(this).attr("href") + lastPart });
            
            $('.link').each(function() {
                var className = $(this).text();
                // set the href by searching the sidebar using class name
                if (className) {
                    $(this).attr("href", getURL(className));
                }             
            })
        } else {
            // edit/child or new term page
            $('.edit_term').attr("action", function() { return $(this).attr("action") + lastPart });

            $('.child_term').attr("action", function() { return $(this).attr("action") + lastPart });
            // add record option when there is nothing selected in broader term
            var record = new Option("record", "record");
            $('.options option').each(function() {
                if ($(this).is(':selected')) {
                $(this).parent().siblings(".selected-options").append("<span class='badge bg-secondary'>"+$(this).text()+'</span>');
            }})
            // if on the child term page the corresponding broader term must be selected
            if (pathname.indexOf("child_term")>=0) {
                $("#broader option:selected").attr('disabled', 'disabled');
            }
            if ($('.selected-broaders').children().length > 0){
                // selected terms field's initialization for the terms which have related broader term
                $("#datstype option[value='record']").remove();
                $("#contexttype").prop('disabled', true);
                $("#contexttype_lbl").hide();
                $("#contexttype").hide();
            } else {
                // initialization for the datatype field when it is a parent term whose type must be Record
                $("#datstype").val('record').change();
                $("#datstype").prop('disabled',true);
                $("#contexttype").prop('disabled', false);
                $("#contexttype_lbl").show();
                $("#contexttype").show();
            }
        }
    }
});
// highlight the highest level item
$(document).ready(function(){
    $(".item-collapsed").on("click", function(){
        $(this).parents('li').last().children('div').children('#term-name').addClass('toggeled');
        if ($(this).attr('aria-expanded')=='false') {
            $(this).parents('li').last().children('div').children('#term-name').removeClass('toggeled');
        }
    });
  });
// enable tooltips
const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
// avoid using ctrl-click in multi-select field and implemented the selected term section
$(".options").mousedown(function(e){
    e.preventDefault();
    var select = this;
    var scroll = select.scrollTop;

    e.target.selected = !e.target.selected;
    setTimeout(function(){select.scrollTop = scroll;}, 0);
    $(select).focus();
    // implement the selected terms field
    $(".selected-options").empty();
    $('.options option').each(function() {
        if($(this).is(':selected')) {
        $(this).parent().siblings(".selected-options").append("<span class='badge bg-secondary'>"+$(this).text()+'</span>');
        }})
    // highest level should have data type Record, make datatype unable to choose if there is no broader term
    var record = new Option("record", "record");
    if ($('.selected-broaders').children().length > 0) {
        // something selected in broader term
        $("#datstype").prop('disabled', false);
        $("#datstype option[value='record']").remove();
        $("#datstype").val(' ').change();
        $("#contexttype").prop('disabled', true);
        $("#contexttype_lbl").hide();
        $("#contexttype").hide();
    } else {
        // nothing selected in broader term
        $("#datstype").append(record);
        $("#datstype").val('record').change();
        $("#datstype").prop('disabled', true);
        $("#contexttype").prop('disabled', false);
        $("#contexttype_lbl").show();
        $("#contexttype").show();
        
    }
}).mousemove(function(e){e.preventDefault()});
// form can be completely posted when its broader term is disabled
$('form').bind('submit', function () {
    $(this).find('#broader option:selected').prop('disabled', false);
    $(this).find("#datstype").prop('disabled', false);
  });
// search field for multiple selection
$(document).ready(function(){
$(".form-filter").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(this).siblings(".options").children().filter(function() {
    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});
});
$(".comment-area").on("keydown", function(event) {
    if((event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey || event.metaKey) {
        $('.comment-btn').click();
    }
});
// show less / show more for tree list
$(document).ready(function(){
    $('span.individual-term:nth-child(-n+6)').show();
    $(".show").click(function(){
        if($(this).text() == "...show more"){ 
            $(this).parent('div').children('span.individual-term').show();
            $(this).text("show less");
        } else {
            $(this).parent('div').children('span.individual-term:nth-child(n+7)').hide();
            $(this).text("...show more");
        }
    });
});
//An anthorization satatus request every ten seconds
const authorization_status_url = "/authorization_status";
    var intervalID = window.setInterval(function() {
        if (window.location.pathname != "/login/login_prompt") {
            fetch(authorization_status_url)
            .then(response => response.json())
            .then(data => {
                if (data["authorized"] === false) {
                    //show modal when login is invalid
                    var modal = document.getElementById("myModal");
                    modal.style.display = "block";
                    clearInterval(intervalID);
                }
            });
        }
    }, 15000);   

var modal = document.getElementById("myModal");
var span = document.getElementsByClassName("close")[0];

span.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

var banner = document.getElementsByClassName("banner")[0];
var closeBanner = document.getElementsByClassName("banner-x")[0];

if (sessionStorage.closeBanner) {
    banner.style.display = "none";
    $('.navbar').css("margin-top", "0px");
    $('body').css("padding-top", "64px");
}

closeBanner.onclick = function() {
    localStorage.setItem("closeBanner", "true");
    sessionStorage.closeBanner = true;
    $('.navbar').css("margin-top", "0px");
    $('body').css("padding-top", "64px");
    banner.style.display = "none";
}

if (localStorage.getItem("agreeTermsApp") === null){
    $('#agree-modal1').css("display", "block")
}

$('.agree-btn1').on("click", function() {
    $('#agree-modal1').css("display", "none");
    $('#agree-modal2').css("display", "block");
});

$('.agree-btn2').on("click", function() {
    $('#agree-modal2').css("display", "none");
    $('#agree-modal3').css("display", "block");
});

$('.agree-btn3').on("click", function() {
    $('#agree-modal3').css("display", "none");
    localStorage.setItem("agreeTermsApp", "true");
});