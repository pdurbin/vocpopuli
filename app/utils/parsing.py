"""
Utility functions for parsing data about terms
in the vocabulary.
"""

from typing import Dict
from markdown import markdown
from uuid import UUID
from flask_dance.contrib.gitlab import gitlab
from ..gitlab_api.api import Endpoints
import re
import base64
from os.path import splitext
from os import environ
import requests


def markdown_to_dict(term_definition: str) -> Dict:
    """
    Converts the markdown definition of a given term,
    taken from its respective GitLab issue, into a
    Python dictionary.
    """

    form_data_dict = dict()

    # extract the term's label
    label_start_idx = (term_definition.find('<p><strong>Label:</strong> ')
                       + len('<p><strong>Label:</strong> '))
    label_end_idx = term_definition.find('</p>', label_start_idx)
    label = term_definition[label_start_idx:label_end_idx]
    form_data_dict['label'] = label

    # extract the term's definition
    definition_start_idx = (
        term_definition.find('<p><strong>Definition:</strong> ')
        + len('<p><strong>Definition:</strong> ')
                            )
    definition_end_idx = term_definition.find('</p>', definition_start_idx)
    definition = term_definition[definition_start_idx:definition_end_idx]
    form_data_dict['definition'] = definition

    # extract the term's synonyms
    synonyms_ul_start_idx = (
        term_definition
        .find('<p><strong>Synonyms (with language tags):</strong> <ul>')
        + len('<p><strong>Synonyms (with language tags):</strong> <ul>')
        )
    synonyms_ul_end_idx = term_definition.find('</ul></p>',
                                               synonyms_ul_start_idx)
    synonyms_ul = term_definition[synonyms_ul_start_idx:synonyms_ul_end_idx]
    synonyms = ', '.join(x
                         for x in re.findall(
                             "<li>(.*?)</li>", synonyms_ul, re.DOTALL
                             )
                         )
    form_data_dict['synonyms'] = synonyms.split(', ')

    # extract the term's datatype
    datatype_start_idx = (
        term_definition.find('<p><strong>Data type:</strong> ')
        + len('<p><strong>Data type:</strong> ')
        )
    datatype_end_idx = term_definition.find('</p>', datatype_start_idx)
    datatype = term_definition[datatype_start_idx:datatype_end_idx]
    form_data_dict['datatype'] = datatype

    # extract the term's contextual type
    context_start_idx = (
        term_definition.find('<p><strong>Contextual type:</strong> ')
        + len('<p><strong>Contextual type:</strong> ')
        )
    context_end_idx = term_definition.find('</p>', context_start_idx)
    contextual_type = term_definition[context_start_idx:context_end_idx]
    if contextual_type not in ['object', 'process', 'data']:
        contextual_type = ''
    form_data_dict['contextual_type'] = contextual_type

    # extract the term's broader terms
    bt_ul_start_idx = (
        term_definition
        .find('<p><strong>Relatively broader terms:</strong> <ul>')
        + len('<p><strong>Relatively broader terms:</strong> <ul>')
        )
    bt_ul_end_idx = term_definition.find('</ul></p>', bt_ul_start_idx)
    bt_ul = term_definition[bt_ul_start_idx:bt_ul_end_idx]
    broader_terms = [x for x in re.findall("<li>(.*?)</li>", bt_ul, re.DOTALL)]
    form_data_dict['broader'] = broader_terms

    # extract the term's broader terms' labels
    btl_ul_start_idx = (
        term_definition
        .find('<p><strong>Relatively broader terms (labels):</strong> <ul>')
        + len('<p><strong>Relatively broader terms (labels):</strong> <ul>')
        )
    btl_ul_end_idx = term_definition.find('</ul></p>', btl_ul_start_idx)
    btl_ul = term_definition[btl_ul_start_idx:btl_ul_end_idx]
    broader_terms_labels = [x for x in re.findall("<li>(.*?)</li>", btl_ul, re.DOTALL)]
    form_data_dict['broader_labels'] = broader_terms_labels

    # extract the term's non-hierarchically related terms
    rt_ul_start_idx = (
        term_definition
        .find('<p><strong>Related terms (non-hierarchical):</strong> <ul>')
        + len('<p><strong>Related terms (non-hierarchical):</strong> <ul>')
        )
    rt_ul_end_idx = term_definition.find('</ul></p>', rt_ul_start_idx)
    rt_ul = term_definition[rt_ul_start_idx:rt_ul_end_idx]
    related_terms = [x for x in re.findall("<li>(.*?)</li>", rt_ul, re.DOTALL)]
    form_data_dict['related'] = related_terms

    # extract the term's non-hierarchically related terms' labels
    rtl_ul_start_idx = (
        term_definition
        .find('<p><strong>Related terms (non-hierarchical) (labels):</strong> <ul>')
        + len('<p><strong>Related terms (non-hierarchical) (labels):</strong> <ul>')
        )
    rtl_ul_end_idx = term_definition.find('</ul></p>', rtl_ul_start_idx)
    rtl_ul = term_definition[rtl_ul_start_idx:rtl_ul_end_idx]
    related_terms_labels = [x for x in re.findall("<li>(.*?)</li>", rtl_ul, re.DOTALL)]
    form_data_dict['related_labels'] = related_terms_labels

    rt_external_ul_start_idx = (
        term_definition
        .find('<p><strong>Related external terms:</strong> <ul>')
        + len('<p><strong>Related external terms:</strong> <ul>')
        )
    rt_external_ul_end_idx = term_definition.find('</ul></p>', rt_external_ul_start_idx)
    rt_external_ul = term_definition[rt_external_ul_start_idx:rt_external_ul_end_idx]
    related_external_terms = [x for x in re.findall("<li>(.*?)</li>", rt_external_ul, re.DOTALL)
                              if x != ""]
    form_data_dict['related_external'] = related_external_terms

    # extract the image links:
    img_ul_start_idx = (
        term_definition
        .find('<p><strong>Term images:</strong> <ul>')
        + len('<p><strong>Term images:</strong> <ul>')
        )
    img_ul_end_idx = term_definition.find('</ul></p>', img_ul_start_idx)
    img_ul = term_definition[img_ul_start_idx:img_ul_end_idx]
    image_links = [url_to_img_tag(i, x)
                   for i, x in
                   enumerate(re.findall("<li>(.*?)</li>", img_ul, re.DOTALL))]
    image_links = [x for x in image_links if x is not None]
    form_data_dict['Image files'] = image_links

    # extract the term's id_t_global
    id_t_global_start_idx = (
        term_definition.find('<p><strong>Global Term ID:</strong> ')
        + len('<p><strong>Global Term ID:</strong> ')
                            )
    id_t_global_end_idx = term_definition.find('</p>', id_t_global_start_idx)
    id_t_global = term_definition[id_t_global_start_idx:id_t_global_end_idx]
    form_data_dict['id_t_global'] = id_t_global

    # extract the term's local ID
    id_t_local_start_idx = (
        term_definition.find('<p><strong>Local Term ID:</strong> ')
        + len('<p><strong>Local Term ID:</strong> ')
                            )
    id_t_local_end_idx = term_definition.find('</p>', id_t_local_start_idx)
    id_t_local = term_definition[id_t_local_start_idx:id_t_local_end_idx]
    try:
        UUID(id_t_local[:-3], version=4)
    except ValueError:
        id_t_local = ""
    form_data_dict['id_t_local'] = id_t_local

    # extract the term's version ID
    id_t_version_start_idx = (
        term_definition.find('<p><strong>Term Version ID:</strong> ')
        + len('<p><strong>Term Version ID:</strong> ')
                            )
    id_t_version_end_idx = term_definition.find('</p>', id_t_version_start_idx)
    id_t_version = term_definition[id_t_version_start_idx:id_t_version_end_idx]
    form_data_dict['id_t_version'] = id_t_version

    # get purl and gitlab_raw_url if these exist
    purl_start_idx = (term_definition.find('<p><strong>PURL:</strong> ')
                      + len('<p><strong>PURL:</strong> '))
    purl_end_idx = term_definition.find('</p>', purl_start_idx)
    purl = term_definition[purl_start_idx:purl_end_idx]
    if 'http' in purl:
        form_data_dict['purl'] = purl

    url_start_idx = (term_definition.find('<p><strong>GitLab SKOS URL:</strong> ')
                     + len('<p><strong>GitLab SKOS URL:</strong> '))
    url_end_idx = term_definition.find('</p>', url_start_idx)
    url = term_definition[url_start_idx:url_end_idx]
    if 'http' in url:
        form_data_dict['gitlab_raw_url'] = url

    return form_data_dict


def new_term_to_markdown(term_dict: Dict) -> str:
    """
    Convert the data from a term's JSON into a
    markdown string which is used as the description
    of the term's GitLab issue.
    """
    term_data = term_dict.get('term')
    term_metadata = term_dict.get('metadata')
    image_files = term_dict.get('image_files', [])

    markdown_str = ""
    markdown_str += markdown(f"**Label:** {term_data['label']}")
    markdown_str += markdown(f"**Definition:** {term_data['definition']}")
    synonyms_list = "<ul>"
    for s in term_data['synonyms']:
        if s == '':
            continue
        synonyms_list += f"<li>{s.strip()}</li>"
    synonyms_list += "</ul>"

    md_synonyms = f"**Synonyms (with language tags):** {synonyms_list}"
    markdown_str += markdown(md_synonyms)

    markdown_str += markdown(f"**Data type:** {term_data['datatype']}")
    markdown_str += markdown(f"**Contextual type:** {term_data.get('contextual_type', ' ')}")

    broader_list = "<ul>"
    for b in term_data['broader']:
        if b == ' ':
            continue
        broader_list += f"<li>{b}</li>"
    broader_list += "</ul>"

    md_broader = f"**Relatively broader terms:** {broader_list}"
    markdown_str += markdown(md_broader)

    broader_labels_list = "<ul>"
    for bl in term_data['broader_labels']:
        if bl == ' ':
            continue
        broader_labels_list += f"<li>{bl}</li>"
    broader_labels_list += "</ul>"

    md_broader_labels = f"**Relatively broader terms (labels):** {broader_labels_list}"
    markdown_str += markdown(md_broader_labels)

    related_list = "<ul>"
    for r in term_data['related']:
        if r == ' ':
            continue
        related_list += f"<li>{r}</li>"
    related_list += "</ul>"

    md_related = f"**Related terms (non-hierarchical):** {related_list}"
    markdown_str += markdown(md_related)

    related_labels_list = "<ul>"
    for rl in term_data['related_labels']:
        if rl == ' ':
            continue
        related_labels_list += f"<li>{rl}</li>"
    related_labels_list += "</ul>"

    md_related_labels = f"**Related terms (non-hierarchical) (labels):** {related_labels_list}"
    markdown_str += markdown(md_related_labels)

    related_external_list = "<ul>"
    for rex in term_data['related_external']:
        if rex == ' ':
            continue
        related_external_list += f"<li>{rex}</li>"
    related_external_list += "</ul>"

    md_related_external = f"**Related external terms:** {related_external_list}"
    markdown_str += markdown(md_related_external)

    img_files_list = "<ul>"
    for i in image_files:
        if i == ' ':
            continue
        img_files_list += f"<li>{i}</li>"
    img_files_list += "</ul>"
    md_img_files = f"**Term images:** {img_files_list}"
    markdown_str += markdown(md_img_files)

    markdown_str += markdown(f"**Global Term ID:** {term_metadata.get('id_t_global', '')}")
    markdown_str += markdown(f"**Local Term ID:** {term_metadata.get('id_t_local', '')}")
    markdown_str += markdown(f"**Term Version ID:** {term_metadata.get('id_t_version', '')}")

    return markdown_str


def pre_jsonify_form(form_data: Dict) -> Dict:
    """
    Some light preprocessing of the submitted
    form data for a new or edited term.
    """
    processed_dict = {}
    for k, v in form_data.items():
        # skip hidden/unneeded fields
        if k in ['submit', 'csrf_token', 'images', 'keep_prev_imgs']:
            continue

        if isinstance(v, list):
            if k in ['broader', 'related']:
                id_t_globals = [x.split(' | ')[0] for x in v]
                labels = [x.split(' | ')[1] for x in v]
                processed_dict[f"{k}"] = id_t_globals
                processed_dict[f"{k}_labels"] = labels
            else:
                # remove empty list elements
                processed_dict[k] = [x for x in v
                                     if ((x != ' ') and (x != "") and (x != " "))]

        # turn a string of comma-separated values into a list
        elif k in ['synonym', 'related_external']:
            if k == 'synonym':
                k_new = 'synonyms'
            else:
                k_new = k
            processed_dict[k_new] = [x.strip()
                                     for x in form_data[k].split(',')]
        # take the field's value as-is
        else:
            processed_dict[k] = v

    return processed_dict


def url_to_img_tag(i: int, img_url: str):
    """
    Converts an img_url into an image tag which can be
    embedded using the view_term view function.

    img_url should be of the form:
    'https://gitlab.com/NAMESPACE/PROJ_NAME/-/raw/BRANCH_NAME/images/IMG_FILE.XXX'
    """
    try:
        img_url_split = img_url.split('-/raw/')[1]
        branch_name = img_url_split.split('/images/')[0]
        file_name = img_url_split.split('/images/')[1]
        file_name = '%2E'.join(file_name.rsplit('.', 1))
        extension = splitext(file_name)[1]
        extension = extension[1:]

        # try-except as a workaround for using the celery
        # background task
        try:
            ep = Endpoints()
            img = gitlab.get(f"{ep.REPO_FILES_EP}/"
                             f"images%2F{file_name}/raw?ref={branch_name}")
        except RuntimeError:
            img = requests.get(f"https://gitlab.com/api/v4/{environ.get('REPO_FILES_EP')}/"
                               f"images%2F{file_name}/raw?ref={branch_name}",
                               params={'private_token': environ.get('VOCPOPULI_TOKEN')})
        img = base64.b64encode(img.content).decode('ascii')

        img_tag = (i, img, extension)
    except IndexError:
        img_tag = None

    return img_tag
