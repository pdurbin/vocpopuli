import dotenv
import os
import git
import requests
from .. import DOTENV_PATH

# load the acces token from the .env file
dotenv.load_dotenv(DOTENV_PATH)

def load_repositories():
    # the main directory of all vocabularies
    VOCABULARIES_DIR = (os.environ.get('VOCABULARIES_DIR') or
                        os.path.abspath('vocabulary_repositories'))

    # check if the vocab directory exists
    if not os.path.exists(VOCABULARIES_DIR):
        os.mkdir(VOCABULARIES_DIR)

    # parameters used in the API calls
    params = {'private_token': os.environ.get('VOCPOPULI_TOKEN'),
              'membership': True,  # for projects_response
              'merged': False,
              'per_page': 100}

    projects_response = requests.get("https://gitlab.com/api/v4/projects",
                                     params=params)
    projects_dict_list = projects_response.json()
    vocabulary_repositories = []

    # load all repositories which the admin user has access to
    # and stoir their relevant information
    # as a dict in vocabulary_repositories
    for repo in projects_dict_list:
        repo_id = repo['id']
        repo_tree_response = requests.get(f'https://gitlab.com/api/v4/projects/{repo_id}/repository/tree',
                                          params=params)

        top_level_paths = [x['path']
                           for x in repo_tree_response.json()]
        if 'approved_terms' in top_level_paths and 'candidate_terms' in top_level_paths:
            repository_branches_response = requests.get(f'https://gitlab.com/api/v4/projects/{repo_id}/repository/branches',
                                                        params=params)
            branches_dict_list = repository_branches_response.json()

            # handle pagination
            for page in range(2, 1 + int(repository_branches_response.headers['X-Total-Pages'])):
                next_page = requests.get(f'https://gitlab.com/api/v4/projects/{repo_id}/repository/branches',
                                         params={'merged': False,
                                                 'per_page': 100,
                                                 'page': page})
                branches_dict_list += next_page.json()
            http_url_to_repo = repo['http_url_to_repo'].replace('://gitlab.',
                                                                f'://{os.environ.get("VOCPOPULI_ADMIN_USER_ID")}:{os.environ.get("VOCPOPULI_TOKEN")}@gitlab.')
            vocabulary_repositories.append({'http_url_to_repo': http_url_to_repo,
                                            'name': repo['name'].replace(' ', '-'),
                                            'branches': [x['name'] for x in branches_dict_list]})

    # Clone all repository branches,
    # which have not already been added to the directory
    for repository_dict in vocabulary_repositories:
        # the directory of the given repository
        # it contains subdirectories for each non-merged repository branch
        repository_dir = os.path.join(VOCABULARIES_DIR, repository_dict['name'])
        if not os.path.exists(repository_dir):
            os.mkdir(repository_dir)

        # clone each non-merged branch
        for branch in repository_dict['branches']:
            branch_dir = os.path.join(repository_dir,
                                      f"{repository_dict['name']}-{branch}")
            if not os.path.exists(branch_dir):
                os.mkdir(branch_dir)
                repo = git.Repo.clone_from(url=repository_dict['http_url_to_repo'],
                                           to_path=branch_dir)
                repo.git.checkout(branch)
