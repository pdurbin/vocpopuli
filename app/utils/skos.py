from rdflib.namespace import RDF, SKOS, XSD, DCTERMS
from rdflib import Graph, Literal, Namespace
from rdflib.term import URIRef
from typing import List, Dict

a = RDF.type


class SKOSSerializer:

    def __init__(self, term_jsons: List[Dict],
                 id_v_global: str,
                 vocab_name: str = None,
                 namespace: str = "https://purls.helmholtz-metadaten.de/vp/",
                 namespace_abbrev: str = "vp",
                 created=None,
                 publisher=None,
                 license_url=None,
                 contributor_names=None):

        self.terms = term_jsons
        self.vocabulary_graph = Graph()

        self.namespace = Namespace(namespace)
        # self.vocabulary_graph.add((self.namespace['hasDataType'],
        #                            a,
        #                            OWL.ObjectProperty))

        # self.vocabulary_graph.bind(namespace_abbrev, self.namespace)
        self.vocabulary_graph.bind('skos', SKOS)
        self.vocabulary_graph.bind('dcterms', DCTERMS)

        # define the concept scheme
        self.id_v_global = id_v_global
        self.ns_concept_scheme = self.namespace[id_v_global]
        self.vocabulary_graph.add((self.ns_concept_scheme,
                                   a,
                                   SKOS.ConceptScheme))
        if vocab_name:
            self.vocabulary_graph.add((self.ns_concept_scheme,
                                       SKOS.prefLabel,
                                       Literal(vocab_name, lang='en')))
        if publisher:
            self.vocabulary_graph.add((self.ns_concept_scheme,
                                       DCTERMS.publisher,
                                       Literal(publisher)))
        if created:
            self.vocabulary_graph.add((self.ns_concept_scheme,
                                       DCTERMS.created,
                                       Literal(created,
                                               datatype=XSD.dateTimeStamp)))
        if license_url:
            self.vocabulary_graph.add((self.ns_concept_scheme,
                                       DCTERMS.license,
                                       URIRef(license_url)))
        if contributor_names:
            for name in contributor_names:
                self.vocabulary_graph.add((self.ns_concept_scheme,
                                           DCTERMS.contributor,
                                           Literal(name)))

    @staticmethod
    def _to_camelcase(_string: str) -> str:
        string = _string
        # string = re.sub("[^a-zA-Z0-9]+", " ", _string)  # remove non-letters
        # string = string.split()
        # string = string[0] + ''.join(i.capitalize() for i in string[1:])
        return string

    @staticmethod
    def _parse_synonyms(synonyms: List[str]) -> List[List[str]]:
        parsed_synonyms = []

        if not synonyms:
            return [[]]
        if len(synonyms) == 1 and synonyms[0] == '':
            return [[]]

        for s in synonyms:
            if '@' in s:
                syn, lang = s.split('@')
            else:
                syn, lang = s, 'en'

            parsed_synonyms.append([syn, lang])

        return parsed_synonyms

    def serialize_term(self, format: str = "ttl") -> str:
        if len(self.terms) > 1:
            return ""
        else:
            self.vocabulary_graph = Graph()

            self.vocabulary_graph.bind('skos', SKOS)

            for term_dict in self.terms:
                # add the term (denoted by id_t_global) to the namespace
                term_iri = f"{self.id_v_global}/{term_dict['id_t_global']}"
                ns_term = self.namespace[term_iri]

                label = term_dict['label']
                if '@' in label[-3:]:
                    label_lang = label[-2:]
                else:
                    label_lang = 'en'
                label_langs = [label_lang]

                # add the term to the vocabulary graph and the concept scheme
                self.vocabulary_graph.add((ns_term, a, SKOS.Concept))
                self.vocabulary_graph.add((ns_term,
                                           SKOS.inScheme,
                                           self.ns_concept_scheme))
                # add its prefLabel
                self.vocabulary_graph.add((ns_term,
                                           SKOS.prefLabel,
                                           Literal(term_dict['label'],
                                                   lang=label_lang)))

                # add its synonyms (prefLabel or altLabel)
                synonyms = self._parse_synonyms(term_dict['synonyms'])
                for synonym in synonyms:
                    if not synonym:
                        continue
                    syn, lang = synonym
                    # if new translation -> prefLabel
                    if lang not in label_langs:
                        self.vocabulary_graph.add((ns_term,
                                                   SKOS.prefLabel,
                                                   Literal(syn, lang=lang)))
                        label_langs.append(lang)
                    # if existing translation -> altLabel
                    else:
                        self.vocabulary_graph.add((ns_term,
                                                   SKOS.altLabel,
                                                   Literal(syn, lang=lang)))

                # add its definition
                self.vocabulary_graph.add((ns_term,
                                           SKOS.definition,
                                           Literal(term_dict['definition'],
                                                   lang='en')))

                # add its broader, and related terms
                for broader in term_dict['broader']:
                    self.vocabulary_graph.add((ns_term,
                                               SKOS.broader,
                                               self.namespace[f"{self.id_v_global}/{broader}"]))

                for related in term_dict['related']:
                    self.vocabulary_graph.add((ns_term,
                                               SKOS.related,
                                               self.namespace[f"{self.id_v_global}/{related}"]))
                for related_external in term_dict['related_external']:
                    self.vocabulary_graph.add((ns_term,
                                               SKOS.relatedMatch,
                                               URIRef(related_external)))

                # add its IDs
                self.vocabulary_graph.add((ns_term,
                                           SKOS.notation,
                                           Literal(term_dict['id_t_version'],
                                                   datatype=XSD.string)))
                if len(term_dict['id_t_local']) > 1:
                    self.vocabulary_graph.add((ns_term,
                                               SKOS.notation,
                                               Literal(term_dict['id_t_local'],
                                                       datatype=XSD.string)))

                # add its datatype
                if term_dict['datatype'] != ' ':
                    self.vocabulary_graph.add((ns_term,
                                               SKOS.scopeNote,
                                               Literal(term_dict['datatype'],
                                                       lang='en')))

            return self.vocabulary_graph.serialize(format=format)

    def serialize_vocabulary(self, format: str = "ttl") -> str:
        if len(self.terms) <= 0:
            return ''

        # begin with the SKOS serialization of the vocabulary
        for term_dict in self.terms:

            # add the term (denoted by id_t_global) to the namespace
            term_iri = f"{self.id_v_global}/{term_dict['id_t_global']}"
            ns_term = self.namespace[term_iri]

            label = term_dict['label']
            if '@' in label[-3:]:
                label_lang = label[-2:]
            else:
                label_lang = 'en'
            label_langs = [label_lang]

            # add the term to the vocabulary graph and the concept scheme
            self.vocabulary_graph.add((ns_term, a, SKOS.Concept))
            self.vocabulary_graph.add((ns_term,
                                       SKOS.inScheme,
                                       self.ns_concept_scheme))
            # add its prefLabel
            self.vocabulary_graph.add((ns_term,
                                       SKOS.prefLabel,
                                       Literal(term_dict['label'],
                                               lang=label_lang)))

            # add its synonyms (prefLabel or altLabel)
            synonyms = self._parse_synonyms(term_dict['synonyms'])
            for synonym in synonyms:
                if not synonym:
                    continue
                syn, lang = synonym
                # if new translation -> prefLabel
                if lang not in label_langs:
                    self.vocabulary_graph.add((ns_term,
                                               SKOS.prefLabel,
                                               Literal(syn, lang=lang)))
                    label_langs.append(lang)
                # if existing translation -> altLabel
                else:
                    self.vocabulary_graph.add((ns_term,
                                               SKOS.altLabel,
                                               Literal(syn, lang=lang)))

            # add its definition
            self.vocabulary_graph.add((ns_term,
                                       SKOS.definition,
                                       Literal(term_dict['definition'],
                                               lang='en')))

            # add its broader, and related terms
            # if a term doesn't have broader terms, it's a top term
            if len(term_dict['broader']) == 0:

                self.vocabulary_graph.add((self.ns_concept_scheme,
                                           SKOS.hasTopConcept,
                                           ns_term))

            for broader in term_dict['broader']:
                self.vocabulary_graph.add((self.namespace[f"{self.id_v_global}/{broader}"],
                                           a,
                                           SKOS.Concept))
                self.vocabulary_graph.add((ns_term,
                                           SKOS.broader,
                                           self.namespace[f"{self.id_v_global}/{broader}"]))

            for related in term_dict['related']:
                self.vocabulary_graph.add((self.namespace[f"{self.id_v_global}/{related}"],
                                           a,
                                           SKOS.Concept))
                self.vocabulary_graph.add((ns_term,
                                           SKOS.related,
                                           self.namespace[f"{self.id_v_global}/{related}"]))
            for related_external in term_dict['related_external']:
                self.vocabulary_graph.add((URIRef(related_external),
                                           a,
                                           SKOS.Concept))
                self.vocabulary_graph.add((ns_term,
                                           SKOS.relatedMatch,
                                           URIRef(related_external)))

            # add its IDs
            self.vocabulary_graph.add((ns_term,
                                       SKOS.notation,
                                       Literal(term_dict['id_t_version'],
                                               datatype=XSD.string)))
            if len(term_dict['id_t_local']) > 1:
                self.vocabulary_graph.add((ns_term,
                                           SKOS.notation,
                                           Literal(term_dict['id_t_local'],
                                                   datatype=XSD.string)))

            # add its datatype
            if term_dict['datatype'] != ' ':
                self.vocabulary_graph.add((ns_term,
                                           SKOS.scopeNote,
                                           Literal(term_dict['datatype'],
                                                   lang='en')))

        return self.vocabulary_graph.serialize(format=format)
