"""
Functions related to the definition and/or extraction
of vocabulary terms.
"""

from typing import Dict, List
from .parsing import markdown_to_dict
from uuid import uuid4
from ..gitlab_api import api
from ..gitlab_api import repository_endpoints
import re
ep = repository_endpoints.Endpoints


def term_tag(term_label: str) -> str:
    """
    Converts the term's label into a
    3-part string which has the following structure:

    'xxx_xxx_xxx'

    Each triple of x's is replaced by the
    first three letters of each of the three
    first words of the term's label (if these exist).

    If the term's label is too short, the x's
    act as placehodlers in the string.

    Non-alphanumeric characters are disregarded.

    Examples:
    term_tag('Cup Grinding Process') = 'cup_gri_pro'
    term_tag('Specimen Cleaning') = 'spe_cle_xxx'
    term_tag('IAM') = 'iam_xxx_xxx'
    term_tag('EM') = 'emx_xxx_xxx'
    term_tag('V^(-1)/%') = 'v1x_xxx_xxx'
    """
    term_tag_str = 'xxx_xxx_xxx'

    if term_label is None:
        return term_tag_str

    term_label = term_label.lower()
    # consider only the first three words:
    term_label_parts = term_label.split()[:3]
    term_label_parts = [''.join([i for i in x if i.isalnum()])
                        for x in term_label_parts]
    term_label_parts = [re.sub(r'[^\x00-\x7f]', r'', x)
                        for x in term_label_parts]

    for i, part in enumerate(term_label_parts):
        if len(part) >= 3:
            part = part[:3]
        elif len(part) == 2:
            part = part + 'x'
        elif len(part) == 1:
            part = part + 'xx'
        else:
            part = 'xxx'

        term_tag_str = term_tag_str.replace('xxx', part, 1)

    return term_tag_str


def generate_uid() -> str:
    "Generates an UUID uid for a given term"

    uid = uuid4()
    return uid.hex


def get_term_jsons() -> List[Dict]:
    """
    Extracts a list of the JSONs of all
    vocabulary terms.
    """

    # get all repository issues
    issues = api.get_issues_list(
            params={'scope': 'all',  # get all issues in the project
                    'order_by': 'created_at',
                    'sort': 'desc',
                    'per_page': 100
                    }
            )

    all_jsons = []

    # get the list of dicts of the approved terms from the respective issues
    # each dict containts the term's key-value pairs, its issue iid, and
    # its approval status
    approved_terms_dicts = [{**markdown_to_dict(x['description']),
                             **{'issue_iid': x['iid']},
                             **{'approved': True}} for x in issues
                            if x['state'] == 'closed'
                            and 'approved' in x['labels']]
    all_jsons += approved_terms_dicts

    # repeat the above step for all candidate terms
    candidate_terms_dicts = [{**markdown_to_dict(x['description']),
                              **{'issue_iid': x['iid']},
                              **{'approved': False}} for x in issues
                             if x['state'] == 'opened'
                             and 'approved' not in x['labels']]

    # only take the most recent edit into account
    for id_t_global in set([x['id_t_global'] for x in candidate_terms_dicts]):
        candidate_dicts = [x for x in candidate_terms_dicts
                           if x['id_t_global'] == id_t_global]
        all_jsons.append(candidate_dicts[0])

    return all_jsons


