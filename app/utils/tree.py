"""
Contains functions related to the generation
of the vocabulary tree from the individual term jsons
"""

from typing import Dict, List
import hashlib
from anytree import Node
from anytree.exporter import JsonExporter
from flask import session


def generate_tree(vocabulary_terms: List[Dict],
                  children_list: List[List],
                  parents_list: List = ['Vocabulary'],
                  tree_node: Node = None,
                  project_id=None) -> str:
    """
    A recursive function used to create a tree represantion
    of the vocabulary.

    vocabulary_terms --- A list of the dictionaries of all terms
    in the vocabulary.

    children_list --- The children nodes which need to be added to
    the parent nodes in ``parents_list`` in the current iteration
    of the function. Each parent is mapped to separate list in
    ``children_list``.

    parents_list --- The parent nodes in the current interation.

    tree_node --- An anytree Node variable containing the structure
    of the tree in the current iteration.
    """
    id_t_locals = [x.get('id_t_local', '') for x in vocabulary_terms
                   if x['approved']]
    id_t_locals = sorted([x for x in id_t_locals if x != ""])
    id_t_locals_str = ', '.join(x for x in id_t_locals)
    n_approved_str = f'{len(id_t_locals):06}'

    id_t_versions = sorted([x.get('id_t_version', '') for x in vocabulary_terms])
    id_t_versions_str = ', '.join(x for x in id_t_versions)

    if id_t_locals_str != '':
        id_v_local = (n_approved_str + '-' +
                      (hashlib.md5(id_t_locals_str.encode('utf-8')).hexdigest()) +
                      '-vp')
    else:
        id_v_local = (n_approved_str + '-' + 'n/a' + '-vp')

    id_v_version = ((hashlib.md5(id_t_versions_str.encode('utf-8'))
                    .hexdigest()) + '-vp')

    if tree_node is not None:  # root has already been constructed
        # append children nodes to each parent node
        for i, parent in enumerate(parents_list):
            node_children_list = []
            for child in children_list[i]:
                # extract the child term's dictionary
                child_dict = [x
                              for x in vocabulary_terms
                              if x['id_t_global'] == child][0]
                # extract the child node's attributes
                approved = child_dict['approved'] or False
                color = "green" if approved else "red"
                issue_iid = child_dict['issue_iid'] or -1
                if child_dict['datatype'] != ' ':
                    datatype = child_dict['datatype']
                else:
                    datatype = False
                definition = child_dict['definition']
                related = child_dict['related']
                id_t_global = child_dict.get('id_t_global', '')
                id_t_local = child_dict.get('id_t_local', '')
                id_t_version = child_dict.get('id_t_version', '')
                related_external = child_dict['related_external']
                contextual_type = child_dict.get('contextual_type', '')
                purl = child_dict.get('purl', None)

                node_children_list.append(Node(name=child_dict['label'],
                                               definition=definition,
                                               id_t_global=id_t_global,
                                               id_t_local=id_t_local,
                                               id_t_version=id_t_version,
                                               approved=approved,
                                               related=related,
                                               related_external=related_external,
                                               issue_iid=issue_iid,
                                               color=color,
                                               datatype=datatype,
                                               contextual_type=contextual_type,
                                               purl=purl))

            parent.children = node_children_list

    else:  # create the root node and add its initial children
        if project_id is not None:
            id_v_global = 'vocab-' + str(project_id) + '-vp'
        else:
            id_v_global = ('vocab-' +
                           str(session.get('PROJECT_ID', 'n/a')) +
                           '-vp')
        tree_node = Node(parents_list[0],
                         id_v_global=id_v_global,
                         id_v_local=id_v_local,
                         id_v_version=id_v_version,
                         approved=True,
                         issue_iid=-1)
        node_children_list = []

        for child in children_list[0]:
            # extract the child term's dictionary
            child_dict = [x
                          for x in vocabulary_terms
                          if x['id_t_global'] == child][0]

            # extract the child node's attributes
            approved = child_dict['approved'] or False
            issue_iid = child_dict['issue_iid'] or -1
            color = "green" if approved else "red"
            if child_dict['datatype'] != ' ':
                datatype = child_dict['datatype']
            else:
                datatype = False
            definition = child_dict['definition']
            related = child_dict['related']
            id_t_global = child_dict.get('id_t_global', '')
            id_t_local = child_dict.get('id_t_local', '')
            id_t_version = child_dict.get('id_t_version', '')
            related_external = child_dict['related_external']
            contextual_type = child_dict.get('contextual_type', '')
            purl = child_dict.get('purl', None)
            node_children_list.append(Node(name=child_dict['label'],
                                           definition=definition,
                                           id_t_global=id_t_global,
                                           id_t_local=id_t_local,
                                           id_t_version=id_t_version,
                                           approved=approved,
                                           related=related,
                                           related_external=related_external,
                                           issue_iid=issue_iid,
                                           color=color,
                                           datatype=datatype,
                                           contextual_type=contextual_type,
                                           purl=purl))

        tree_node.children = node_children_list

    # set the parent and children nodes for the next iteration
    parents_next_iter = [x for x in tree_node.leaves]
    children_next_iter = [[x['id_t_global']
                           for x in vocabulary_terms
                           if parent.id_t_global in x['broader']]
                          for parent in parents_next_iter]

    if not any(children_next_iter):  # halting condition
        return JsonExporter(indent=4).export(tree_node)
    else:
        return generate_tree(vocabulary_terms=vocabulary_terms,
                             children_list=children_next_iter,
                             parents_list=parents_next_iter,
                             tree_node=tree_node)
