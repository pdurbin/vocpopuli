"""
Contains the different app configurations
used in VocPopuli
"""

import os
import ssl


class Config:
    OAUTH_CLIENT_ID = os.environ.get('OAUTH_CLIENT_ID')

    OAUTH_CLIENT_SECRET = os.environ.get('OAUTH_CLIENT_SECRET')

    SECRET_KEY = os.environ.get('SECRET_KEY')

    VOCABULARIES_DIR = (os.environ.get('VOCABULARIES_DIR') or
                        os.path.abspath('vocabulary_repositories'))

    # Flask-Session
    SESSION_TYPE = os.environ.get('SESSION_TYPE') or 'filesystem'
    SESSION_PERMANENT = os.environ.get('SESSION_PERMANENT') or True
    SESSION_USE_SIGNER = os.environ.get('SESSION_USE_SIGNER') or True

    # Celery
    CELERY_BROKER_URL = os.environ['REDIS_URL']
    result_backend = os.environ['REDIS_URL']
    broker_use_ssl = {'ssl_cert_reqs': ssl.CERT_NONE}
    redis_backend_use_ssl = {'ssl_cert_reqs': ssl.CERT_NONE}

    @staticmethod
    def init_app():
        pass


class DevelopmentConfig(Config):
    DEBUG = True

    @staticmethod
    def init_app():
        OAUTHLIB_INSECURE_TRANSPORT = (
            os.environ.get('OAUTHLIB_INSECURE_TRANSPORT') or '1'
            )
        os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = OAUTHLIB_INSECURE_TRANSPORT


class TestingConfig(Config):
    TESTING = True


class ProductionConfig(Config):
    pass


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig
}
