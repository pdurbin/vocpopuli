import os
from app import create_app
from app.utils.repo import load_repositories

load_repositories()
app = create_app(os.environ.get('FLASK_CONFIG') or 'default')

if not os.path.exists('.env'):
    with open('.env', 'w'):
        pass


@app.cli.command()
def test():
    """Run the unit tests in the 'tests' package"""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)
